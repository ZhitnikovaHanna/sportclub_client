﻿namespace SportClub_Client.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.people_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.role_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.программаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сменаПароляToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.абонементыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.услугиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.услугиВАбонементахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скидкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.работникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тренажерыИСнарядыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.клиентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.абонементыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.клиентыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.расписаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инвентарьКлубаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетПоКлиентамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетПоСотрудникамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.услугиКлубаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.клубныеКартыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.count_dgv = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.использоватьУслугуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.abon_dgv = new System.Windows.Forms.DataGridView();
            this.history_dgv = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ok_btn = new System.Windows.Forms.Button();
            this.disc_txbx = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.service_cmbx = new System.Windows.Forms.ComboBox();
            this.clients_cmbx = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.search_btn = new System.Windows.Forms.Button();
            this.exit_btn = new System.Windows.Forms.Button();
            this.инвентарьКлубаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.count_dgv)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abon_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.history_dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.people_status,
            this.toolStripStatusLabel2,
            this.role_status});
            this.statusStrip1.Location = new System.Drawing.Point(0, 707);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(141, 17);
            this.toolStripStatusLabel1.Text = "Текущий пользователь: ";
            // 
            // people_status
            // 
            this.people_status.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.people_status.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.people_status.Name = "people_status";
            this.people_status.Size = new System.Drawing.Size(23, 17);
            this.people_status.Text = "<>";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(37, 17);
            this.toolStripStatusLabel2.Text = "Роль:";
            // 
            // role_status
            // 
            this.role_status.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.role_status.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.role_status.Name = "role_status";
            this.role_status.Size = new System.Drawing.Size(23, 17);
            this.role_status.Text = "<>";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.программаToolStripMenuItem,
            this.справочникиToolStripMenuItem,
            this.клиентыToolStripMenuItem,
            this.расписаниеToolStripMenuItem,
            this.инвентарьКлубаToolStripMenuItem,
            this.отчетыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 69;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // программаToolStripMenuItem
            // 
            this.программаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сменаПароляToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.программаToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_kservices_15660;
            this.программаToolStripMenuItem.Name = "программаToolStripMenuItem";
            this.программаToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.программаToolStripMenuItem.Text = "Программа";
            // 
            // сменаПароляToolStripMenuItem
            // 
            this.сменаПароляToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_status_45418;
            this.сменаПароляToolStripMenuItem.Name = "сменаПароляToolStripMenuItem";
            this.сменаПароляToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.сменаПароляToolStripMenuItem.Text = "Смена пароля";
            this.сменаПароляToolStripMenuItem.Click += new System.EventHandler(this.сменаПароляToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.New_Window_24;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.абонементыToolStripMenuItem,
            this.услугиToolStripMenuItem,
            this.услугиВАбонементахToolStripMenuItem,
            this.скидкиToolStripMenuItem,
            this.работникиToolStripMenuItem,
            this.тренажерыИСнарядыToolStripMenuItem});
            this.справочникиToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_history_45350;
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(110, 20);
            this.справочникиToolStripMenuItem.Text = "Справочники";
            // 
            // абонементыToolStripMenuItem
            // 
            this.абонементыToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_Credit_Card_10997;
            this.абонементыToolStripMenuItem.Name = "абонементыToolStripMenuItem";
            this.абонементыToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.абонементыToolStripMenuItem.Text = "Абонементы";
            this.абонементыToolStripMenuItem.Click += new System.EventHandler(this.абонементыToolStripMenuItem_Click);
            // 
            // услугиToolStripMenuItem
            // 
            this.услугиToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_Sport_table_tennis_70661;
            this.услугиToolStripMenuItem.Name = "услугиToolStripMenuItem";
            this.услугиToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.услугиToolStripMenuItem.Text = "Услуги";
            this.услугиToolStripMenuItem.Click += new System.EventHandler(this.услугиToolStripMenuItem_Click);
            // 
            // услугиВАбонементахToolStripMenuItem
            // 
            this.услугиВАбонементахToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_alacarte_30045;
            this.услугиВАбонементахToolStripMenuItem.Name = "услугиВАбонементахToolStripMenuItem";
            this.услугиВАбонементахToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.услугиВАбонементахToolStripMenuItem.Text = "Услуги в абонементах";
            this.услугиВАбонементахToolStripMenuItem.Click += new System.EventHandler(this.услугиВАбонементахToolStripMenuItem_Click);
            // 
            // скидкиToolStripMenuItem
            // 
            this.скидкиToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_1_11_511563;
            this.скидкиToolStripMenuItem.Name = "скидкиToolStripMenuItem";
            this.скидкиToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.скидкиToolStripMenuItem.Text = "Скидки";
            this.скидкиToolStripMenuItem.Click += new System.EventHandler(this.скидкиToolStripMenuItem_Click);
            // 
            // работникиToolStripMenuItem
            // 
            this.работникиToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_school_events_45188;
            this.работникиToolStripMenuItem.Name = "работникиToolStripMenuItem";
            this.работникиToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.работникиToolStripMenuItem.Text = "Работники";
            this.работникиToolStripMenuItem.Click += new System.EventHandler(this.работникиToolStripMenuItem_Click);
            // 
            // тренажерыИСнарядыToolStripMenuItem
            // 
            this.тренажерыИСнарядыToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_16_2104700;
            this.тренажерыИСнарядыToolStripMenuItem.Name = "тренажерыИСнарядыToolStripMenuItem";
            this.тренажерыИСнарядыToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.тренажерыИСнарядыToolStripMenuItem.Text = "Тренажеры и снаряды";
            this.тренажерыИСнарядыToolStripMenuItem.Click += new System.EventHandler(this.тренажерыИСнарядыToolStripMenuItem_Click);
            // 
            // клиентыToolStripMenuItem
            // 
            this.клиентыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.абонементыToolStripMenuItem1,
            this.клиентыToolStripMenuItem1});
            this.клиентыToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_run_15543;
            this.клиентыToolStripMenuItem.Name = "клиентыToolStripMenuItem";
            this.клиентыToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.клиентыToolStripMenuItem.Text = "Клиенты";
            // 
            // абонементыToolStripMenuItem1
            // 
            this.абонементыToolStripMenuItem1.Image = global::SportClub_Client.Properties.Resources.if_Sport_dumbbell_70654;
            this.абонементыToolStripMenuItem1.Name = "абонементыToolStripMenuItem1";
            this.абонементыToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.абонементыToolStripMenuItem1.Text = "Абонементы";
            this.абонементыToolStripMenuItem1.Click += new System.EventHandler(this.абонементыToolStripMenuItem1_Click);
            // 
            // клиентыToolStripMenuItem1
            // 
            this.клиентыToolStripMenuItem1.Image = global::SportClub_Client.Properties.Resources.if_run_15543;
            this.клиентыToolStripMenuItem1.Name = "клиентыToolStripMenuItem1";
            this.клиентыToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.клиентыToolStripMenuItem1.Text = "Клиенты";
            this.клиентыToolStripMenuItem1.Click += new System.EventHandler(this.клиентыToolStripMenuItem_Click);
            // 
            // расписаниеToolStripMenuItem
            // 
            this.расписаниеToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_alacarte_30045;
            this.расписаниеToolStripMenuItem.Name = "расписаниеToolStripMenuItem";
            this.расписаниеToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.расписаниеToolStripMenuItem.Text = "Расписание";
            this.расписаниеToolStripMenuItem.Click += new System.EventHandler(this.расписаниеToolStripMenuItem_Click);
            // 
            // инвентарьКлубаToolStripMenuItem
            // 
            this.инвентарьКлубаToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_volleyball_27581;
            this.инвентарьКлубаToolStripMenuItem.Name = "инвентарьКлубаToolStripMenuItem";
            this.инвентарьКлубаToolStripMenuItem.Size = new System.Drawing.Size(129, 20);
            this.инвентарьКлубаToolStripMenuItem.Text = "Инвентарь клуба";
            this.инвентарьКлубаToolStripMenuItem.Click += new System.EventHandler(this.инвентарьКлубаToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отчетПоКлиентамToolStripMenuItem,
            this.отчетПоСотрудникамToolStripMenuItem,
            this.услугиКлубаToolStripMenuItem,
            this.клубныеКартыToolStripMenuItem,
            this.инвентарьКлубаToolStripMenuItem1});
            this.отчетыToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_gnome_mime_application_vnd_lotus_1_2_3_30343;
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // отчетПоКлиентамToolStripMenuItem
            // 
            this.отчетПоКлиентамToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_gnome_mime_application_vnd_lotus_1_2_3_30343;
            this.отчетПоКлиентамToolStripMenuItem.Name = "отчетПоКлиентамToolStripMenuItem";
            this.отчетПоКлиентамToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.отчетПоКлиентамToolStripMenuItem.Text = "Отчет по клиентам";
            this.отчетПоКлиентамToolStripMenuItem.Click += new System.EventHandler(this.отчетПоКлиентамToolStripMenuItem_Click);
            // 
            // отчетПоСотрудникамToolStripMenuItem
            // 
            this.отчетПоСотрудникамToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_gnome_mime_application_vnd_lotus_1_2_3_30343;
            this.отчетПоСотрудникамToolStripMenuItem.Name = "отчетПоСотрудникамToolStripMenuItem";
            this.отчетПоСотрудникамToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.отчетПоСотрудникамToolStripMenuItem.Text = "Отчет по сотрудникам";
            this.отчетПоСотрудникамToolStripMenuItem.Click += new System.EventHandler(this.отчетПоСотрудникамToolStripMenuItem_Click);
            // 
            // услугиКлубаToolStripMenuItem
            // 
            this.услугиКлубаToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_gnome_mime_application_vnd_lotus_1_2_3_30343;
            this.услугиКлубаToolStripMenuItem.Name = "услугиКлубаToolStripMenuItem";
            this.услугиКлубаToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.услугиКлубаToolStripMenuItem.Text = "Услуги клуба";
            this.услугиКлубаToolStripMenuItem.Click += new System.EventHandler(this.услугиКлубаToolStripMenuItem_Click);
            // 
            // клубныеКартыToolStripMenuItem
            // 
            this.клубныеКартыToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.if_gnome_mime_application_vnd_lotus_1_2_3_30343;
            this.клубныеКартыToolStripMenuItem.Name = "клубныеКартыToolStripMenuItem";
            this.клубныеКартыToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.клубныеКартыToolStripMenuItem.Text = "Клубные карты";
            this.клубныеКартыToolStripMenuItem.Click += new System.EventHandler(this.клубныеКартыToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 74;
            this.label3.Text = "Номер абонемента";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(172, 20);
            this.textBox1.TabIndex = 71;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // count_dgv
            // 
            this.count_dgv.AllowUserToAddRows = false;
            this.count_dgv.AllowUserToDeleteRows = false;
            this.count_dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.count_dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.count_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.count_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.count_dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.count_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.count_dgv.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.count_dgv.DefaultCellStyle = dataGridViewCellStyle8;
            this.count_dgv.Location = new System.Drawing.Point(278, 117);
            this.count_dgv.Name = "count_dgv";
            this.count_dgv.ReadOnly = true;
            this.count_dgv.RowHeadersVisible = false;
            this.count_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.count_dgv.Size = new System.Drawing.Size(718, 200);
            this.count_dgv.TabIndex = 70;
            this.count_dgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.count_dgv_DataBindingComplete);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.использоватьУслугуToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(191, 26);
            // 
            // использоватьУслугуToolStripMenuItem
            // 
            this.использоватьУслугуToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.Down_03_24;
            this.использоватьУслугуToolStripMenuItem.Name = "использоватьУслугуToolStripMenuItem";
            this.использоватьУслугуToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.использоватьУслугуToolStripMenuItem.Text = "Использовать услугу";
            this.использоватьУслугуToolStripMenuItem.Click += new System.EventHandler(this.использоватьУслугуToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(278, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 76;
            this.label1.Text = "Информация об абонементе";
            // 
            // abon_dgv
            // 
            this.abon_dgv.AllowUserToAddRows = false;
            this.abon_dgv.AllowUserToDeleteRows = false;
            this.abon_dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.abon_dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.abon_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.abon_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.abon_dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.abon_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.abon_dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.abon_dgv.Location = new System.Drawing.Point(12, 73);
            this.abon_dgv.Name = "abon_dgv";
            this.abon_dgv.ReadOnly = true;
            this.abon_dgv.RowHeadersVisible = false;
            this.abon_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.abon_dgv.Size = new System.Drawing.Size(260, 631);
            this.abon_dgv.TabIndex = 70;
            this.abon_dgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.abon_dgv_DataBindingComplete);
            this.abon_dgv.SelectionChanged += new System.EventHandler(this.abon_dgv_SelectionChanged);
            // 
            // history_dgv
            // 
            this.history_dgv.AllowUserToAddRows = false;
            this.history_dgv.AllowUserToDeleteRows = false;
            this.history_dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.history_dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.history_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.history_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.history_dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.history_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.history_dgv.DefaultCellStyle = dataGridViewCellStyle10;
            this.history_dgv.Location = new System.Drawing.Point(278, 338);
            this.history_dgv.Name = "history_dgv";
            this.history_dgv.ReadOnly = true;
            this.history_dgv.RowHeadersVisible = false;
            this.history_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.history_dgv.Size = new System.Drawing.Size(718, 200);
            this.history_dgv.TabIndex = 70;
            this.history_dgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.history_dgv_DataBindingComplete);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(278, 322);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "История посещений";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(278, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 76;
            this.label4.Text = "<>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(275, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 76;
            this.label5.Text = "Доступные услуги";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ok_btn);
            this.groupBox1.Controls.Add(this.disc_txbx);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.service_cmbx);
            this.groupBox1.Controls.Add(this.clients_cmbx);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(278, 545);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(718, 100);
            this.groupBox1.TabIndex = 79;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Оформить услугу без регистрации или абонемента";
            // 
            // ok_btn
            // 
            this.ok_btn.Image = global::SportClub_Client.Properties.Resources.OK_24;
            this.ok_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ok_btn.Location = new System.Drawing.Point(459, 29);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(97, 48);
            this.ok_btn.TabIndex = 77;
            this.ok_btn.Text = "ОК";
            this.ok_btn.UseVisualStyleBackColor = true;
            this.ok_btn.Click += new System.EventHandler(this.ok_btn_Click);
            // 
            // disc_txbx
            // 
            this.disc_txbx.Location = new System.Drawing.Point(333, 56);
            this.disc_txbx.Name = "disc_txbx";
            this.disc_txbx.ReadOnly = true;
            this.disc_txbx.Size = new System.Drawing.Size(120, 20);
            this.disc_txbx.TabIndex = 2;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(333, 30);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 1;
            // 
            // service_cmbx
            // 
            this.service_cmbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.service_cmbx.FormattingEnabled = true;
            this.service_cmbx.Location = new System.Drawing.Point(62, 56);
            this.service_cmbx.Name = "service_cmbx";
            this.service_cmbx.Size = new System.Drawing.Size(244, 21);
            this.service_cmbx.TabIndex = 0;
            this.service_cmbx.SelectedIndexChanged += new System.EventHandler(this.service_cmbx_SelectedIndexChanged);
            // 
            // clients_cmbx
            // 
            this.clients_cmbx.FormattingEnabled = true;
            this.clients_cmbx.Location = new System.Drawing.Point(62, 29);
            this.clients_cmbx.Name = "clients_cmbx";
            this.clients_cmbx.Size = new System.Drawing.Size(244, 21);
            this.clients_cmbx.TabIndex = 0;
            this.clients_cmbx.SelectedIndexChanged += new System.EventHandler(this.clients_cmbx_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "Услуга";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 76;
            this.label6.Text = "Клиент";
            // 
            // search_btn
            // 
            this.search_btn.Image = global::SportClub_Client.Properties.Resources.Search_24;
            this.search_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.search_btn.Location = new System.Drawing.Point(190, 43);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(82, 27);
            this.search_btn.TabIndex = 77;
            this.search_btn.Text = "Поиск";
            this.search_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.search_btn.UseVisualStyleBackColor = true;
            this.search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // exit_btn
            // 
            this.exit_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exit_btn.Image = global::SportClub_Client.Properties.Resources.New_Window_24;
            this.exit_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.exit_btn.Location = new System.Drawing.Point(830, 670);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(166, 34);
            this.exit_btn.TabIndex = 68;
            this.exit_btn.Text = "Выход из программы";
            this.exit_btn.UseVisualStyleBackColor = true;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // инвентарьКлубаToolStripMenuItem1
            // 
            this.инвентарьКлубаToolStripMenuItem1.Image = global::SportClub_Client.Properties.Resources.if_gnome_mime_application_vnd_lotus_1_2_3_30343;
            this.инвентарьКлубаToolStripMenuItem1.Name = "инвентарьКлубаToolStripMenuItem1";
            this.инвентарьКлубаToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.инвентарьКлубаToolStripMenuItem1.Text = "Инвентарь клуба";
            this.инвентарьКлубаToolStripMenuItem1.Click += new System.EventHandler(this.инвентарьКлубаToolStripMenuItem1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.search_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.abon_dgv);
            this.Controls.Add(this.history_dgv);
            this.Controls.Add(this.count_dgv);
            this.Controls.Add(this.exit_btn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Система учета спортивного клуба";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.count_dgv)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.abon_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.history_dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel people_status;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel role_status;
        private System.Windows.Forms.Button exit_btn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem программаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сменаПароляToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem абонементыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem услугиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem услугиВАбонементахToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скидкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem работникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem клиентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView count_dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView abon_dgv;
        private System.Windows.Forms.Button search_btn;
        private System.Windows.Forms.DataGridView history_dgv;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem использоватьУслугуToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox disc_txbx;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ComboBox service_cmbx;
        private System.Windows.Forms.ComboBox clients_cmbx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.ToolStripMenuItem отчетПоКлиентамToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетПоСотрудникамToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem услугиКлубаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem клубныеКартыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem расписаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem абонементыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem клиентыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem тренажерыИСнарядыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem инвентарьКлубаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem инвентарьКлубаToolStripMenuItem1;
    }
}