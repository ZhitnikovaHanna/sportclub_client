﻿using SportClub_Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class ClientsForm : Form
    {
        public ClientsForm()
        {
            InitializeComponent();
        }


        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            txbx.Text = string.Empty; //Обнуляем то что в них было
            phone_txbx.Text = string.Empty;
            birth_dtpc.Value = DateTime.Now;
            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = txbx.Enabled = phone_txbx.Enabled = birth_dtpc.Enabled = false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = txbx.Enabled = phone_txbx.Enabled = birth_dtpc.Enabled = true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetClients();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            //Если включен режим редактирования
            if (isEdit)
            {

                //Проверяем, выбран ли элемент для редактирования
                if (dgv.SelectedRows.Count > 0)
                {
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as Client;
                    item.Name = txbx.Text; //заполняем его данными из текстбокса
                    item.Phone = phone_txbx.Text;
                    item.BirthDate = birth_dtpc.Value;
                    Data.DataAccess.UpdateClient(item);
                }
            }
            else
            {
                //Создаем новый объект 
                var item = new Client();
                //Заполняем его
                item.Name = txbx.Text; //заполняем его данными из текстбокса
                item.Phone = phone_txbx.Text;
                item.BirthDate = birth_dtpc.Value;
                //Вставляем запись в БД
                Data.DataAccess.InsertClient(item);
            }
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetClients();
        }

        //Событие возникает после привязки данных, т.е после изменения dgv.DataSource
        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Name"] != null)
                dgv.Columns["Name"].HeaderText = "ФИО клиента";


            if (dgv.Columns["ID_Client"] != null)
                dgv.Columns["ID_Client"].Visible = false;
            if (dgv.Columns["BirthDate"] != null)
                dgv.Columns["BirthDate"].HeaderText = "Дата рождения";
            if (dgv.Columns["ServicesAbonement"] != null)
                dgv.Columns["ServicesAbonement"].Visible = false;
            if (dgv.Columns["AbonemetsClient"] != null)
                dgv.Columns["AbonemetsClient"].Visible = false;

        }

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as Client;

                //Выводим в текстбокс
                txbx.Text = item.Name; //заполняем его данными из текстбокса
                phone_txbx.Text = item.Phone;
                birth_dtpc.Value = item.BirthDate;
                DisableControls();
                isEdit = false;
            }
            else
            {
                txbx.Text = phone_txbx.Text = "";
                birth_dtpc.Value = DateTime.Now;
            }
        }

        //Событие загрузки формы
        private void Form_Load(object sender, EventArgs e)
        {
            //Заполняем таблицу
            dgv.DataSource = DataAccess.GetClients();
            if (DataAccess.employee == null)
            {
                button1.Enabled = false;
            }
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Client;
                DataAccess.DeleteClient(item);

                dgv.DataSource = DataAccess.GetClients();
            }
        }

        private void обновитьДанныкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = DataAccess.GetClients();
        }

        private void search_btn_Click(object sender, EventArgs e)
        {
            if (maskedTextBox1.MaskCompleted)
            {
                dgv.DataSource = DataAccess.GetClients(textBox1.Text, maskedTextBox1.Text);
            }
            else dgv.DataSource = DataAccess.GetClients(textBox1.Text, "");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Client;
                AddAbonementForm aad = new AddAbonementForm(item);
                aad.ShowDialog();
            }
        }
    }
}
