﻿using System;
using System.Windows.Forms;
using SportClub_Client.Data;

namespace SportClub_Client
{
    public partial class LoginForm : Form
    {
        public bool isOK { get; set; }
        public object People { get; set; }

        public LoginForm()
        {
            InitializeComponent();
        }

        //Кнопка ОК
        private void button2_Click(object sender, EventArgs e)
        {
            switch (Role.role)
            {
                
                //Если Роль Менеджер 
                case RoleEnum.Менеджер:
                    if (manager_cmbx.SelectedItem != null)
                    {
                        //Проверяем его пароль
                        if (DataAccess.CheckPassword(Role.role, manager_cmbx.SelectedItem, manager_pass_txbx.Text))
                        {
                            //если все нормально сохраняем ссылку на сотрудника в DataAccess
                            People = manager_cmbx.SelectedItem;
                            isOK = true;
                            DataAccess.employee = manager_cmbx.SelectedItem as Employee;

                            this.Close();
                        }
                        else MessageBox.Show("Пароль не верный!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else MessageBox.Show("Необходимо выбрать свое имя!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                    //если пользователь админ.
                case RoleEnum.Администратор:
                    //проверяем пароль
                    if (DataAccess.CheckPassword(Role.role, null, adminpass_txbx.Text))
                    {
                        //Ссылку не сохр. оставляяя пустой объект
                        People = new object();
                        isOK = true;
                        this.Close();
                    }
                    else MessageBox.Show("Пароль не верный!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                default:
                    break;
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            //Устанавливаем значки для таб контрола
            tabControl1.ImageList = new ImageList();
          //заполняем форму данными
          //устанавливаем значки для таб контролов
            tabControl1.ImageList.Images.Add(Properties.Resources._103850___id_identification_student);
            tabControl1.ImageList.Images.Add(Properties.Resources.if_administrator_43711);
            for (int i = 0; i < tabControl1.TabPages.Count; i++)
            {
                tabControl1.TabPages[i].ImageIndex = i;
            }

          //заполянем комбобокс с менеджерами
            manager_cmbx.DataSource = DataAccess.GetMenagers();

            Role.role = RoleEnum.Менеджер;

        }

        //подсказка для пароля
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Стандартный пароль системы - password. После входа в программу рекомендуется сменить пароль. 
            Если ваш пароль не позволяет войти в программу, обратитесь к администратору системы", "Информация о паролях",
                                                                                                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //при смене вкладки, меняем роль
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedTab.Text)
            {
                case "Менеджер": Role.role = RoleEnum.Менеджер; break;
                case "Администратор": Role.role = RoleEnum.Администратор; break;
            }
        }
    }
}
