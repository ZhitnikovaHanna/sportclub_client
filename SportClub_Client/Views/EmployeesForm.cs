﻿using SportClub_Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class EmployeesForm : Form
    {
        public EmployeesForm()
        {
            InitializeComponent();
        }

        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            txbx.Text = string.Empty; //Обнуляем то что в них было
            phone.Text = string.Empty;
            post_cmbx.SelectedItem = null;
            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = txbx.Enabled = phone.Enabled = post_cmbx.Enabled = false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = txbx.Enabled = phone.Enabled = post_cmbx.Enabled = true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetEmployees();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            //Если включен режим редактирования
            if (isEdit)
            {
      

                //Проверяем, выбран ли элемент для редактирования
                if (dgv.SelectedRows.Count > 0 && post_cmbx.SelectedItem != null)
                {
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as Employee;
                    item.Name = txbx.Text; //заполняем его данными из текстбокса
                    item.Phone = phone.Text;
                    item.Post = post_cmbx.SelectedItem as Post;
                    Data.DataAccess.UpdateEmployee(item);
                }
                else MessageBox.Show("Работник или должность не выбраны", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (post_cmbx.SelectedItem != null)
                {
                    //Создаем новый объект 
                    var item = new Employee();
                    //Заполняем его
                    item.Name = txbx.Text; //заполняем его данными из текстбокса
                    item.Phone = phone.Text;
                    item.Post = post_cmbx.SelectedItem as Post;
                    //Вставляем запись в БД
                    Data.DataAccess.InsertEmployee(item);
                }
                else MessageBox.Show("Должность не выбрана", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetEmployees();
        }

        //Событие возникает после привязки данных, т.е после изменения dgv.DataSource
        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Name"] != null)
                dgv.Columns["Name"].HeaderText = "ФИО работника";

            if (dgv.Columns["Phone"] != null)
                dgv.Columns["Phone"].Visible = false;

            if (dgv.Columns["Password"] != null)
                dgv.Columns["Password"].Visible = false;
            if (dgv.Columns["Schedule"] != null)
                dgv.Columns["Schedule"].Visible = false;
            if (dgv.Columns["GumApparatus"] != null)
                dgv.Columns["GumApparatus"].Visible = false;
            if (dgv.Columns["Post"] != null)
                dgv.Columns["Post"].Visible = false;
            if (dgv.Columns["JournalServices"] != null)
                dgv.Columns["JournalServices"].Visible = false;
            if (dgv.Columns["JournalServices1"] != null)
                dgv.Columns["JournalServices1"].Visible = false;
            if (dgv.Columns["ID_Employee"] != null)
                dgv.Columns["ID_Employee"].Visible = false;

        }

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as Employee;

                //Выводим в текстбокс
                txbx.Text = item.Name;
                phone.Text = item.Phone;
                post_cmbx.SelectedItem = item.Post;

                DisableControls();
                isEdit = false;
            }
        }

        //Событие загрузки формы
        private void Form_Load(object sender, EventArgs e)
        {
            //Заполняем таблицу
            dgv.DataSource = DataAccess.GetEmployees();
            post_cmbx.DataSource = DataAccess.GetPosts();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LecturerForm_Load(object sender, EventArgs e)
        {
            dgv.DataSource = DataAccess.GetEmployees();
        }

        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as Employee;
                DataAccess.DeleteEmployee(item);
                dgv.DataSource = DataAccess.GetEmployees();
            }
        }

    }
}
