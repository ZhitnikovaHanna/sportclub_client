﻿namespace SportClub_Client.Views
{
    partial class GymApparatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.exit_btn = new System.Windows.Forms.Button();
            this.edit_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.place_txbx = new System.Windows.Forms.TextBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.apparatus_cmbx = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.empl_cmbx = new System.Windows.Forms.ComboBox();
            this.date_dtpc = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.status_cmbx = new System.Windows.Forms.ComboBox();
            this.INV_txbx = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.select_btn = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.изменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.обновитьДанныкToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label7 = new System.Windows.Forms.Label();
            this.sEmpl_cmbx = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.sStatus_cmbx = new System.Windows.Forms.ComboBox();
            this.sInv_txbx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.search_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(791, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 72;
            this.label1.Text = "Место установки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(790, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 13);
            this.label2.TabIndex = 73;
            this.label2.Text = "Название тренажера или снаряда";
            // 
            // save_btn
            // 
            this.save_btn.Image = global::SportClub_Client.Properties.Resources.OK_24;
            this.save_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.save_btn.Location = new System.Drawing.Point(793, 636);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(203, 34);
            this.save_btn.TabIndex = 68;
            this.save_btn.Text = "Сохранить";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // exit_btn
            // 
            this.exit_btn.Image = global::SportClub_Client.Properties.Resources.New_Window_24;
            this.exit_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.exit_btn.Location = new System.Drawing.Point(903, 676);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(93, 34);
            this.exit_btn.TabIndex = 69;
            this.exit_btn.Text = "Выход";
            this.exit_btn.UseVisualStyleBackColor = true;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // edit_btn
            // 
            this.edit_btn.Image = global::SportClub_Client.Properties.Resources.Pencil_24;
            this.edit_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.edit_btn.Location = new System.Drawing.Point(903, 596);
            this.edit_btn.Name = "edit_btn";
            this.edit_btn.Size = new System.Drawing.Size(93, 34);
            this.edit_btn.TabIndex = 70;
            this.edit_btn.Text = "Изменить";
            this.edit_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.edit_btn.UseVisualStyleBackColor = true;
            this.edit_btn.Click += new System.EventHandler(this.edit_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Image = global::SportClub_Client.Properties.Resources.Plus_01_66_24;
            this.add_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.add_btn.Location = new System.Drawing.Point(793, 596);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 34);
            this.add_btn.TabIndex = 71;
            this.add_btn.Text = "Добавить";
            this.add_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // place_txbx
            // 
            this.place_txbx.Location = new System.Drawing.Point(793, 151);
            this.place_txbx.Name = "place_txbx";
            this.place_txbx.Size = new System.Drawing.Size(199, 20);
            this.place_txbx.TabIndex = 66;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.Location = new System.Drawing.Point(12, 54);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(772, 656);
            this.dgv.TabIndex = 65;
            this.dgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_DataBindingComplete);
            this.dgv.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // apparatus_cmbx
            // 
            this.apparatus_cmbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apparatus_cmbx.FormattingEnabled = true;
            this.apparatus_cmbx.Location = new System.Drawing.Point(793, 111);
            this.apparatus_cmbx.Name = "apparatus_cmbx";
            this.apparatus_cmbx.Size = new System.Drawing.Size(199, 21);
            this.apparatus_cmbx.TabIndex = 74;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(790, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 13);
            this.label3.TabIndex = 73;
            this.label3.Text = "Отв. сотрудник за состояние снаряда";
            // 
            // empl_cmbx
            // 
            this.empl_cmbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.empl_cmbx.FormattingEnabled = true;
            this.empl_cmbx.Location = new System.Drawing.Point(793, 195);
            this.empl_cmbx.Name = "empl_cmbx";
            this.empl_cmbx.Size = new System.Drawing.Size(199, 21);
            this.empl_cmbx.TabIndex = 74;
            // 
            // date_dtpc
            // 
            this.date_dtpc.Location = new System.Drawing.Point(793, 235);
            this.date_dtpc.Name = "date_dtpc";
            this.date_dtpc.Size = new System.Drawing.Size(199, 20);
            this.date_dtpc.TabIndex = 75;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(794, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 13);
            this.label4.TabIndex = 73;
            this.label4.Text = "Дата последнего обслуживания";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(790, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 73;
            this.label5.Text = "Статус снаряда";
            // 
            // status_cmbx
            // 
            this.status_cmbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.status_cmbx.FormattingEnabled = true;
            this.status_cmbx.Location = new System.Drawing.Point(793, 272);
            this.status_cmbx.Name = "status_cmbx";
            this.status_cmbx.Size = new System.Drawing.Size(199, 21);
            this.status_cmbx.TabIndex = 74;
            // 
            // INV_txbx
            // 
            this.INV_txbx.Location = new System.Drawing.Point(793, 315);
            this.INV_txbx.Name = "INV_txbx";
            this.INV_txbx.Size = new System.Drawing.Size(199, 20);
            this.INV_txbx.TabIndex = 66;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(791, 299);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 72;
            this.label6.Text = "Инв. номер";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(793, 342);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(199, 213);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 76;
            this.pictureBox1.TabStop = false;
            // 
            // select_btn
            // 
            this.select_btn.Location = new System.Drawing.Point(791, 561);
            this.select_btn.Name = "select_btn";
            this.select_btn.Size = new System.Drawing.Size(205, 23);
            this.select_btn.TabIndex = 77;
            this.select_btn.Text = "Выбрать фото";
            this.select_btn.UseVisualStyleBackColor = true;
            this.select_btn.Click += new System.EventHandler(this.select_btn_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.изменитьToolStripMenuItem,
            this.удалитьToolStripMenuItem,
            this.toolStripSeparator1,
            this.обновитьДанныкToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(173, 98);
            this.contextMenuStrip1.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.Plus_01_66_24;
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            // 
            // изменитьToolStripMenuItem
            // 
            this.изменитьToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.Pencil_24;
            this.изменитьToolStripMenuItem.Name = "изменитьToolStripMenuItem";
            this.изменитьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.изменитьToolStripMenuItem.Text = "Изменить";
            this.изменитьToolStripMenuItem.Click += new System.EventHandler(this.edit_btn_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.Remove_24;
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(169, 6);
            // 
            // обновитьДанныкToolStripMenuItem
            // 
            this.обновитьДанныкToolStripMenuItem.Image = global::SportClub_Client.Properties.Resources.Refresh_24;
            this.обновитьДанныкToolStripMenuItem.Name = "обновитьДанныкToolStripMenuItem";
            this.обновитьДанныкToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.обновитьДанныкToolStripMenuItem.Text = "Обновить данные";
            this.обновитьДанныкToolStripMenuItem.Click += new System.EventHandler(this.обновитьДанныкToolStripMenuItem_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(200, 13);
            this.label7.TabIndex = 73;
            this.label7.Text = "Отв. сотрудник за состояние снаряда";
            // 
            // sEmpl_cmbx
            // 
            this.sEmpl_cmbx.FormattingEnabled = true;
            this.sEmpl_cmbx.Location = new System.Drawing.Point(12, 27);
            this.sEmpl_cmbx.Name = "sEmpl_cmbx";
            this.sEmpl_cmbx.Size = new System.Drawing.Size(199, 21);
            this.sEmpl_cmbx.TabIndex = 74;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(218, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 73;
            this.label8.Text = "Статус снаряда";
            // 
            // sStatus_cmbx
            // 
            this.sStatus_cmbx.FormattingEnabled = true;
            this.sStatus_cmbx.Location = new System.Drawing.Point(218, 27);
            this.sStatus_cmbx.Name = "sStatus_cmbx";
            this.sStatus_cmbx.Size = new System.Drawing.Size(199, 21);
            this.sStatus_cmbx.TabIndex = 74;
            // 
            // sInv_txbx
            // 
            this.sInv_txbx.Location = new System.Drawing.Point(423, 27);
            this.sInv_txbx.Name = "sInv_txbx";
            this.sInv_txbx.Size = new System.Drawing.Size(199, 20);
            this.sInv_txbx.TabIndex = 66;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(421, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 72;
            this.label9.Text = "Инв. номер";
            // 
            // search_btn
            // 
            this.search_btn.Image = global::SportClub_Client.Properties.Resources.Search_24;
            this.search_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.search_btn.Location = new System.Drawing.Point(681, 23);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(103, 27);
            this.search_btn.TabIndex = 78;
            this.search_btn.Text = "Поиск";
            this.search_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.search_btn.UseVisualStyleBackColor = true;
            this.search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // GymApparatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.search_btn);
            this.Controls.Add(this.select_btn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.date_dtpc);
            this.Controls.Add(this.sStatus_cmbx);
            this.Controls.Add(this.status_cmbx);
            this.Controls.Add(this.sEmpl_cmbx);
            this.Controls.Add(this.empl_cmbx);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.apparatus_cmbx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.exit_btn);
            this.Controls.Add(this.edit_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.sInv_txbx);
            this.Controls.Add(this.INV_txbx);
            this.Controls.Add(this.place_txbx);
            this.Controls.Add(this.dgv);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "GymApparatusForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Спортинвентарь клуба";
            this.Load += new System.EventHandler(this.GymApparatusForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button exit_btn;
        private System.Windows.Forms.Button edit_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.TextBox place_txbx;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.ComboBox apparatus_cmbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox empl_cmbx;
        private System.Windows.Forms.DateTimePicker date_dtpc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox status_cmbx;
        private System.Windows.Forms.TextBox INV_txbx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button select_btn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem изменитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem обновитьДанныкToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox sEmpl_cmbx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox sStatus_cmbx;
        private System.Windows.Forms.TextBox sInv_txbx;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button search_btn;
    }
}