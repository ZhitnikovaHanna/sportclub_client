﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SportClub_Client.Data;

namespace SportClub_Client.Views
{
    public partial class ApparatusForm : Form
    {
        public ApparatusForm()
        {
            InitializeComponent();
        }

        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            txbx.Text = string.Empty; //Обнуляем то что в них было
            info_txbx.Text = string.Empty;
            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = txbx.Enabled = info_txbx.Enabled  = false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = txbx.Enabled = info_txbx.Enabled  = true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetApparatusList();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            //Если включен режим редактирования
            if (isEdit)
            {
               

                //Проверяем, выбран ли элемент для редактирования
                if (dgv.SelectedRows.Count > 0)
                {
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as Apparatus;
                    item.Title = txbx.Text; //заполняем его данными из текстбокса
                    item.Info  = info_txbx.Text;

                    Data.DataAccess.UpdateApparatus(item);
                }
            }
            else
            {
                //Создаем новый объект 
                var item = new Apparatus();
                //Заполняем его
                item.Title = txbx.Text; //заполняем его данными из текстбокса
                item.Info = info_txbx.Text;
 
                //Вставляем запись в БД
                Data.DataAccess.InsertApparatus(item);
            }
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetApparatusList();
        }

        //Событие возникает после привязки данных, т.е после изменения dgv.DataSource
        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Title"] != null)
                dgv.Columns["Title"].HeaderText = "Название";
            if (dgv.Columns["ID_Apparatus"] != null)
                dgv.Columns["ID_Apparatus"].Visible = false;
            if (dgv.Columns["GumApparatus"] != null)
                dgv.Columns["GumApparatus"].Visible = false;
            if (dgv.Columns["Info"] != null)
                dgv.Columns["Info"].Visible = false;
           
        }

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as Apparatus;

                //Выводим в текстбокс
                txbx.Text = item.Title;
                info_txbx.Text = item.Info;
              

                DisableControls();
                isEdit = false;
            }
        }

        //Событие загрузки формы
        private void Form_Load(object sender, EventArgs e)
        {
            //Заполняем таблицу
            dgv.DataSource = DataAccess.GetApparatusList();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Apparatus;
                DataAccess.DeleteApparatus(item);

                dgv.DataSource = DataAccess.GetApparatusList();
            }
        }

        private void обновитьДанныкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = DataAccess.GetApparatusList();
        }

    }
}
