﻿using SportClub_Client.Data;
using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class MainForm : Form
    {
        public MainForm(object people)
        {
            InitializeComponent();
            People = people;
        }

        public object People { get; set; }

        //загрузка формы
        private void MainForm_Load(object sender, EventArgs e)
        {
            switch (Role.role)
            {
                //В зависимости от роли вкл. или выключаем пункты куда пользователь не сможе заходить
                case RoleEnum.Администратор:
                    people_status.Text = "Администратор системы";
                    role_status.Text = Role.role.ToString();
                    использоватьУслугуToolStripMenuItem.Enabled = false;
                    people_status.ForeColor = role_status.ForeColor = Color.Red;
                    ok_btn.Enabled = false;

                    break;
                case RoleEnum.Менеджер:
                    people_status.Text = (People as Employee).Name;
                    role_status.Text = Role.role.ToString();
                    отчетыToolStripMenuItem.Enabled = false;
                    работникиToolStripMenuItem.Enabled = false;
                    скидкиToolStripMenuItem.Enabled = false;
                    абонементыToolStripMenuItem.Enabled = услугиToolStripMenuItem.Enabled =
                        услугиВАбонементахToolStripMenuItem.Enabled = false; скидкиToolStripMenuItem.Enabled = false;
                    break;
                default:
                    break;
            }
            //заполняем табл. с абонементами
            abon_dgv.DataSource = DataAccess.GetAbonementsClients();
            //Заполняем комбобокс с клиентами
            clients_cmbx.DataSource = DataAccess.GetClients();
            //Реализуем автодополнение ввода для удобства поиска клиента
            clients_cmbx.AutoCompleteCustomSource = new AutoCompleteStringCollection();
            foreach (var item in DataAccess.GetClients())
            {
                clients_cmbx.AutoCompleteCustomSource.Add(item.Name);
            }
            clients_cmbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            clients_cmbx.AutoCompleteSource = AutoCompleteSource.CustomSource;
            //заполняем комбобокс с услугами
            service_cmbx.DataSource = DataAccess.GetServices();

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region открытие закрытие форм 
        private void абонементыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbonementForm af = new AbonementForm();
            af.ShowDialog();
            abon_dgv.DataSource = DataAccess.GetAbonementsClients();
        }

        private void услугиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServiceForm sf = new ServiceForm();
            sf.ShowDialog();
        }

        private void клиентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClientsForm cf = new ClientsForm();
            cf.ShowDialog();
            abon_dgv.DataSource = DataAccess.GetAbonementsClients();
        }

        private void работникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeesForm ef = new EmployeesForm();
            ef.ShowDialog();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void скидкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DiscountRatesForm df = new DiscountRatesForm();
            df.ShowDialog();
            abon_dgv.DataSource = DataAccess.GetAbonementsClients();
        }

        private void услугиВАбонементахToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServicesInAbonemtnsForm saf = new ServicesInAbonemtnsForm();
            saf.ShowDialog();
            abon_dgv.DataSource = DataAccess.GetAbonementsClients().Where(x=>x.Status == true).ToList();
        }
        #endregion
        //поиск конкретного абонемента
        private void search_btn_Click(object sender, EventArgs e)
        {
            abon_dgv.DataSource = DataAccess.GetAbonementsClients(textBox1.Text);
        }
        //переименовываем колонки
        private void abon_dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (abon_dgv.Columns["ID_AbonemetClient"] != null)
                abon_dgv.Columns["ID_AbonemetClient"].HeaderText = "Номер";
            if (abon_dgv.Columns["Abonement"] != null)
                abon_dgv.Columns["Abonement"].HeaderText = "Абонемент";

            if (abon_dgv.Columns["Client"] != null)
                abon_dgv.Columns["Client"].Visible = false;
            if (abon_dgv.Columns["Employee"] != null)
                abon_dgv.Columns["Employee"].Visible = false;
            if (abon_dgv.Columns["DateStart"] != null)
                abon_dgv.Columns["DateStart"].Visible = false;
            if (abon_dgv.Columns["DateEnd"] != null)
                abon_dgv.Columns["DateEnd"].Visible = false;
            if (abon_dgv.Columns["Employee_ID"] != null)
                abon_dgv.Columns["Employee_ID"].Visible = false;
            if (abon_dgv.Columns["JournalServices"] != null)
                abon_dgv.Columns["JournalServices"].Visible = false;
            if (abon_dgv.Columns["Status"] != null)
                abon_dgv.Columns["Status"].HeaderText = "Активирован";
        }
        //при каждом выборе абонемента из таблицы
        private void abon_dgv_SelectionChanged(object sender, EventArgs e)
        {
            //выводим всю информацию о нем
            if (abon_dgv.SelectedRows.Count > 0)
            {
                var item = abon_dgv.SelectedRows[0].DataBoundItem as AbonemetsClient;

                label4.Text = string.Format("Клиент: {0}. Дата действия абонемента {1} - {2}", item.Client.Name,
                    item.DateStart.Date.ToShortDateString(), item.DateEnd.Date.ToShortDateString());

                if (item.DateEnd < DateTime.Now.Date)
                {
                    label4.Text += " ПРОСРОЧЕН!";
                    label4.ForeColor = Color.IndianRed;
                }
                else label4.ForeColor = Color.Black;


                if (item.Status == false)
                {
                    label4.Text += " Заморожен!";
                    label4.ForeColor = Color.IndianRed;
                }

                
                //выводим доступные услуги абонемента и его историю
                count_dgv.DataSource = DataAccess.GetAviableService(item);
                history_dgv.DataSource = DataAccess.GetJournal(item);
            }
            else
            {
                label4.Text = "";
                count_dgv.DataSource = null;
                history_dgv.DataSource = null;
            }
        }
        //переименовываем колонки
        private void count_dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (count_dgv.Columns["service"] != null)
                count_dgv.Columns["service"].HeaderText = "Услуга";
            if (count_dgv.Columns["count"] != null)
                count_dgv.Columns["count"].HeaderText = "Доступное кол-во";
        }
        //переименовываем колонки
        private void history_dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (history_dgv.Columns["DateUse"] != null)
                history_dgv.Columns["DateUse"].HeaderText = "Дата";
            if (history_dgv.Columns["Cost"] != null)
                history_dgv.Columns["Cost"].HeaderText = "Оплачено";
            if (history_dgv.Columns["AbonemetsClient"] != null)
                history_dgv.Columns["AbonemetsClient"].Visible = false;
            if (history_dgv.Columns["ID_JournalService"] != null)
                history_dgv.Columns["ID_JournalService"].Visible = false;
            if (history_dgv.Columns["Employee"] != null)
                history_dgv.Columns["Employee"].HeaderText = "Менеджер";
            if (history_dgv.Columns["Employee1"] != null)
                history_dgv.Columns["Employee1"].HeaderText = "Тренер";
            if (history_dgv.Columns["Service"] != null)
            {
                history_dgv.Columns["Service"].HeaderText = "Услуга";
                history_dgv.Columns["Service"].DisplayIndex = 1;
            }

            if (history_dgv.Columns["DiscountClient"] != null)
                history_dgv.Columns["DiscountClient"].HeaderText = "Скидка";
        }

        private void использоватьУслугуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (count_dgv.SelectedRows.Count > 0)
            {
                var item = count_dgv.SelectedRows[0].DataBoundItem as AviableServices;
                if (item.count > 0)
                {
                    SelectTrenerForm sf = new SelectTrenerForm();
                    sf.FormClosing += Sf_FormClosing;
                    sf.ShowDialog();
                }

                else MessageBox.Show("Услуги данного типа в абонементе закночились", "Операция прервана", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //после закрытия формы, где указывалась услуга и тренер обновляем все таблицы, связанные
        private void Sf_FormClosing(object sender, FormClosingEventArgs e)
        {
            var item = count_dgv.SelectedRows[0].DataBoundItem as AviableServices;
            var f = sender as SelectTrenerForm;
            if (f.isOK)
            {
                var item1 = abon_dgv.SelectedRows[0].DataBoundItem as AbonemetsClient;
                DataAccess.UseService(item, f.employee); //используем услугу
                //обновляем таблицы
                history_dgv.DataSource = DataAccess.GetJournal(item.id_abonementclient);
                count_dgv.DataSource = DataAccess.GetAviableService(item1);
            }
        }
        //при выборе клиента меняем скидку в зависимости от того, сколько на его счету
        private void clients_cmbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (clients_cmbx.SelectedItem != null)
            {
                var client = clients_cmbx.SelectedItem as Client;
                disc_txbx.Text = DataAccess.GetDiscountClient(client).ToString();
            }
            else disc_txbx.Text = "0";
        }

        //рассчет стоимости услуг в зависимости от скидки
        private void service_cmbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (service_cmbx.SelectedItem != null)
            {
                var serv = service_cmbx.SelectedItem as Service;
                numericUpDown1.Value = serv.Price - serv.Price * decimal.Parse(disc_txbx.Text) / 100;
            }
            else numericUpDown1.Value = 0;
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            SelectTrenerForm sf = new SelectTrenerForm();
            sf.FormClosing += Sf_FormClosing1;
            sf.ShowDialog();
        }

        private void Sf_FormClosing1(object sender, FormClosingEventArgs e)
        {
            var f = sender as SelectTrenerForm;
            if (f.isOK)
            {
                if (service_cmbx.SelectedItem != null && clients_cmbx.SelectedItem != null)
                {
                    var cl = clients_cmbx.SelectedItem as Client;
                    var serv = service_cmbx.SelectedItem as Service;
                    var disc = decimal.Parse(disc_txbx.Text);
                    DataAccess.AddToJournal(cl, serv, numericUpDown1.Value, disc, f.employee);
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Данные об оказании услуги сохранены:");
                    sb.AppendLine(string.Format("Клиент - {0}", cl.Name));
                    sb.AppendLine(string.Format("Оказанная услуга - {0}", serv.Title));
                    sb.AppendLine(string.Format("Стоимость услуги - {0}", serv.Price));
                    sb.AppendLine(string.Format("Скидка клиента - {0}", disc));
                    sb.AppendLine(string.Format("Оплачено - {0}", serv.Price - serv.Price * disc / 100));
                    sb.AppendLine(string.Format("Тренер - {0}", f.employee.Name));
                    MessageBox.Show(sb.ToString(), "Операция прошла успешно", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show("Поля клиент и услуга не могут быть пустыми. Если клиент не зарегистрирован в системе - необходимо выбрать пункт <Не зарегистрированный клиент>");
            }
        }

#region открытие закрытие разных форм
        private void услугиКлубаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reportForm rf = new reportForm("ServicesReport");
            rf.ShowDialog();
        }

        private void клубныеКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reportForm rf = new reportForm("AbonementsReport");
            rf.ShowDialog();
        }

        private void отчетПоСотрудникамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatePickerForm dpf = new DatePickerForm();
            dpf.FormClosing += dpf_FormClosing;
            dpf.ShowDialog();
        }

        void dpf_FormClosing(object sender, FormClosingEventArgs e)
        {
            var f = sender as DatePickerForm;
            if (f.start != f.end)
            {
                reportForm rf = new reportForm("EmployeeReport",f.start,f.end);
                rf.ShowDialog();
            }

        }

        private void отчетПоКлиентамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatePickerForm dpf = new DatePickerForm();
            dpf.FormClosing += dpf_FormClosing1;
            dpf.ShowDialog();
        }

        private void dpf_FormClosing1(object sender, FormClosingEventArgs e)
        {
            var f = sender as DatePickerForm;
            if (f.start != f.end)
            {
                reportForm rf = new reportForm("ClientReport", f.start, f.end);
                rf.ShowDialog();
            }
        }

        private void расписаниеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScheduleForm sf = new ScheduleForm();
            sf.ShowDialog();
        }

        private void абонементыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AbonementsForm af = new AbonementsForm();
            af.ShowDialog();
            abon_dgv.DataSource = DataAccess.GetAbonementsClients().Where(x => x.Status == true).ToList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void тренажерыИСнарядыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApparatusForm af = new ApparatusForm();
            af.ShowDialog();
        }

        private void инвентарьКлубаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GymApparatusForm gaf = new GymApparatusForm();
            gaf.ShowDialog();
        }

        private void инвентарьКлубаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            reportForm rf = new reportForm("GymApparatusReport");
            rf.ShowDialog();
        }

        private void сменаПароляToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (People as Employee != null)
            {
                ChangePassword_frm cpf = new ChangePassword_frm(People as Employee);
                cpf.ShowDialog();
            }
            else MessageBox.Show("Изменение пароля администратора запрещено!");
            
        }
#endregion

    }
}
