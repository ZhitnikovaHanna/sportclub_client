﻿using SportClub_Client.Data;
using System;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class AddAbonementForm : Form
    {
        private readonly Client client;

        public AddAbonementForm(Client client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddAbonementForm_Load(object sender, EventArgs e)
        {
            abonement_cmbx.DataSource = DataAccess.GetAbonements();
            disc_num.Value = DataAccess.GetDiscountClient(client);
        }

        private void abonement_cmbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (abonement_cmbx.SelectedItem != null)
            {
                var item = abonement_cmbx.SelectedItem as Abonement;
                amount_num.Value = item.Price;
                dateTimePicker2.Value = dateTimePicker1.Value.Date.AddMonths(item.CountMonth);
            }
            else amount_num.Value = 0;
        }

        private void amount_num_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown3.Value = amount_num.Value - amount_num.Value * disc_num.Value / 100;
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            if (abonement_cmbx.SelectedItem != null)
            {
                var item = abonement_cmbx.SelectedItem as Abonement;
                var abonement = DataAccess.AddAbonementClient(client, item, dateTimePicker1.Value.Date, dateTimePicker2.Value.Date );
                MessageBox.Show("Операция успешна");
                this.Close();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (abonement_cmbx.SelectedItem != null)
            {
                var item = abonement_cmbx.SelectedItem as Abonement;
                dateTimePicker2.Value = dateTimePicker1.Value.Date.AddMonths(item.CountMonth);
            }

        }
    }
}
