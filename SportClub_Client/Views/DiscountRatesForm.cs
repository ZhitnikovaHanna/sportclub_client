﻿using SportClub_Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class DiscountRatesForm : Form
    {
        public DiscountRatesForm()
        {
            InitializeComponent();
        }


        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            amount_numeric.Value  = 0; //Обнуляем то что в них было
            price_num.Value = 0;
            
            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = amount_numeric.Enabled = price_num.Enabled = false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = amount_numeric.Enabled = price_num.Enabled  = true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetDiscounts();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            //Если включен режим редактирования
            if (isEdit)
            {
           

                //Проверяем, выбран ли элемент для редактирования
                if (dgv.SelectedRows.Count > 0 )
                {
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as DiscountRates;
                    item.Amount = amount_numeric.Value;
                    item.Discount = price_num.Value;
                    Data.DataAccess.UpdatDiscount(item);
                }
             
            }
            else
            {
                
                    //Создаем новый объект 
                    var item = new DiscountRates();
                //Заполняем его
                item.Amount = amount_numeric.Value;
                item.Discount = price_num.Value;
                //Вставляем запись в БД
                Data.DataAccess.InsertDiscount(item);
                

            }
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetDiscounts();
        }

        //Событие возникает после привязки данных, т.е после изменения dgv.DataSource
        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Amount"] != null)
                dgv.Columns["Amount"].HeaderText = "Сумма";

            if (dgv.Columns["Discount"] != null)
                dgv.Columns["Discount"].HeaderText = "Скидка";

     
            if (dgv.Columns["ID_DiscountRates"] != null)
                dgv.Columns["ID_DiscountRates"].Visible = false;

        }

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as DiscountRates;

                //Выводим в текстбокс
                amount_numeric.Value = item.Amount;
                price_num.Value = item.Discount;
                DisableControls();
                isEdit = false;
            }
        }

        //Событие загрузки формы
        private void Form_Load(object sender, EventArgs e)
        {
            //Заполняем таблицу
            dgv.DataSource = DataAccess.GetDiscounts();
        
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     

        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as DiscountRates;
                DataAccess.DeleteDiscount(item);
                dgv.DataSource = DataAccess.GetDiscounts();
            }
        }
    }
}
