﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SportClub_Client.Data;

namespace SportClub_Client.Views
{
    public partial class GymApparatusForm : Form
    {
        List<string> statusList = new List<string>()
        {
            "Исправен", "Требует ревизии", "Не исправен", "На ремонте"
        };

        private byte[] fotoBytes;


        public GymApparatusForm()
        {
            InitializeComponent();
        }

        private void GymApparatusForm_Load(object sender, EventArgs e)
        {
            dgv.DataSource = DataAccess.GetGymAparatus();

            apparatus_cmbx.DataSource = DataAccess.GetApparatusList();
            apparatus_cmbx.SelectedItem = null;

            empl_cmbx.DataSource = DataAccess.GetEmployees();
            empl_cmbx.SelectedItem = null;

            status_cmbx.DataSource = statusList;
            status_cmbx.SelectedItem = null;


            sStatus_cmbx.DataSource = statusList.ToList();
            sEmpl_cmbx.DataSource = DataAccess.GetEmployees();
        }

        private void select_btn_Click(object sender, EventArgs e)
        {
            FileDialog fd = new OpenFileDialog();
            fd.Filter = "Image files| *.jpeg;"
                                     + "*.jpg;"
                                     + "*.bmp;";

            fd.FileOk +=fd_FileOk;

            fd.ShowDialog();
        }

        private void fd_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var fd = sender as OpenFileDialog;
            fotoBytes = File.ReadAllBytes(fd.FileName);

            if (fotoBytes != null)
            {
                pictureBox1.Image = Image.FromStream(new MemoryStream(fotoBytes));
            }
            else pictureBox1.Image = null;
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (dgv.Columns["Status"] != null)
                dgv.Columns["Status"].HeaderText = "Состояние";
            if (dgv.Columns["LastDate"] != null)
                dgv.Columns["LastDate"].HeaderText = "Дата посл. обслуж.";
            if (dgv.Columns["InvNum"] != null)
                dgv.Columns["InvNum"].HeaderText = "Инв. номер";
            
            if (dgv.Columns["Foto"] != null)
                dgv.Columns["Foto"].Visible = false;

            if (dgv.Columns["Apparatus"] != null)
            {
                dgv.Columns["Apparatus"].HeaderText = "Снаряд";
                dgv.Columns["Apparatus"].DisplayIndex = 1;
            }
                
            if (dgv.Columns["Employee"] != null)
                dgv.Columns["Employee"].HeaderText = "Отв. сотрудник";
            if (dgv.Columns["Place"] != null)
                dgv.Columns["Place"].HeaderText = "Расположение";
            if (dgv.Columns["ID_GymApparatus"] != null)
                dgv.Columns["ID_GymApparatus"].Visible = false;

        }

        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            INV_txbx.Text = place_txbx.Text = string.Empty; //Обнуляем то что в них было
            status_cmbx.SelectedItem = apparatus_cmbx.SelectedItem = empl_cmbx.SelectedItem = null;
            date_dtpc.Value = DateTime.Now.Date;
            fotoBytes = null;
            pictureBox1.Image = null;

            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = INV_txbx.Enabled = place_txbx.Enabled = status_cmbx.Enabled = apparatus_cmbx.Enabled = status_cmbx.Enabled = date_dtpc.Enabled = select_btn.Enabled= false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = INV_txbx.Enabled = place_txbx.Enabled = status_cmbx.Enabled = apparatus_cmbx.Enabled = status_cmbx.Enabled = date_dtpc.Enabled = select_btn.Enabled = true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetGymAparatus();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0 && apparatus_cmbx.SelectedItem != null && empl_cmbx.SelectedItem != null && status_cmbx.SelectedItem != null)
            {
            //Если включен режим редактирования
            if (isEdit)
            {
               
                //Проверяем, выбран ли элемент для редактирования
               
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as GumApparatus;

                    item.Apparatus = apparatus_cmbx.SelectedItem as Apparatus;
                    item.Employee = empl_cmbx.SelectedItem as Employee;
                    item.Foto = fotoBytes;
                    item.InvNum = INV_txbx.Text;
                    item.LastDate = date_dtpc.Value.Date;
                    item.Place = place_txbx.Text;
                    item.Status = status_cmbx.Text;

                    Data.DataAccess.UpdateGymApparatus(item);
                }
            
            else
            {
                //Создаем новый объект 
                var item = new GumApparatus();
                //Заполняем его
                item.Apparatus = apparatus_cmbx.SelectedItem as Apparatus;
                item.Employee = empl_cmbx.SelectedItem as Employee;
                item.Foto = fotoBytes;
                item.InvNum = INV_txbx.Text;
                item.LastDate = date_dtpc.Value.Date;
                item.Place = place_txbx.Text;
                item.Status = status_cmbx.Text;

                //Вставляем запись в БД
                Data.DataAccess.InsertGumApparatus(item);
            }
            }
            else MessageBox.Show("Пожалуйста, заполните все поля!", "Отмена операции", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetGymAparatus();

        }
       

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as GumApparatus;

                apparatus_cmbx.SelectedItem = item.Apparatus;
                empl_cmbx.SelectedItem = item.Employee;
                fotoBytes = item.Foto;
                
                INV_txbx.Text = item.InvNum;
                date_dtpc.Value = item.LastDate;
                place_txbx.Text = item.Place;
                status_cmbx.Text = item.Status;

                if (item.Foto != null)
                {
                    pictureBox1.Image = Image.FromStream(new MemoryStream(fotoBytes));
                }
                else pictureBox1.Image = null;

                DisableControls();
                isEdit = false;
            }
        }


        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Apparatus;
                DataAccess.DeleteApparatus(item);

                dgv.DataSource = DataAccess.GetGymAparatus();
            }
        }

        private void обновитьДанныкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = DataAccess.GetGymAparatus();
        }

        private void search_btn_Click(object sender, EventArgs e)
        {
            var query = DataAccess.GetGymAparatus();
            if (sStatus_cmbx.Text.Length >0)
            {
                query = query.Where(x => x.Status == sStatus_cmbx.Text).ToList();
            }
            if (sEmpl_cmbx.SelectedItem != null)
            {
                query = query.Where(x => x.Employee.ID_Employee == (sEmpl_cmbx.SelectedItem as Employee).ID_Employee)
                    .ToList();
            }
            if (sInv_txbx.Text.Length>0)
            {
                query = query.Where(x => x.InvNum.Contains(sInv_txbx.Text)).ToList();
            }

            dgv.DataSource = query;

        }
    }
}
