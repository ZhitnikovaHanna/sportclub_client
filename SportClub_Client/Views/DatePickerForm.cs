﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{

    //Форма для выбора промежутка дат
    public partial class DatePickerForm : Form
    {
        public DateTime start;
        public DateTime end;

        public DatePickerForm()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker2.MinDate = dateTimePicker1.Value.Date;
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            start = dateTimePicker1.Value.Date;
            end = dateTimePicker2.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            this.Close();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
