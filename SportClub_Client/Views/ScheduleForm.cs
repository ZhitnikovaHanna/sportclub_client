﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SportClub_Client.Data;

namespace SportClub_Client.Views
{
    public partial class ScheduleForm : Form
    {
     
        public ScheduleForm()
        {
            InitializeComponent();
        }
        //Получаем расписания из построителя расписания
        private void ScheduleForm_Load(object sender, EventArgs e)
        {
            schedule_dgv.DataSource = ScheduleBuilder.Scedule;

        }

        //заполняем все пикеры с датами, чтобы они были на нулях или на нужных значения (а не на текущем времени)
        void FillDateTimePickers()
        {
            if (schedule_dgv.SelectedRows.Count > 0)
            {
                var item = (schedule_dgv.SelectedRows[0].DataBoundItem as DataRowView).Row;

                for (int i = 2; i < item.Table.Columns.Count; i++)
                {
                    SetTime(item.Table.Columns[i].ColumnName, item[i] as ScheduleTime);
                }

            }
        }

        //При выборе сотрудника
        private void schedule_dgv_SelectionChanged(object sender, EventArgs e)
        {
            //заполняем пикеры
            FillDateTimePickers();

            if (schedule_dgv.SelectedRows.Count > 0)
            {
                var item = (schedule_dgv.SelectedRows[0].DataBoundItem as DataRowView).Row;

                TimeSpan totalTime = new TimeSpan();
                //Подсчитываем сколько он в неделю работал и выводим вниз формы
                for (int i = 2; i < item.Table.Columns.Count; i++)
                {
                    var time = item[i] as ScheduleTime;
                    if (time != null)
                    {
                        totalTime = totalTime.Add((TimeSpan)time.end - (TimeSpan)time.start);
                    }

                }

                hours_lbl.Text = ((int)totalTime.TotalHours).ToString() + "ч. " + totalTime.Minutes.ToString() + " мин.";
            }
        }

        //Проверяем включенные боксы и заполняем их
        void SetTime(string DayOfWeak, ScheduleTime time)
        {
            switch (DayOfWeak)
            {
                case "Понедельник": if (time != null)
                    {
                        monday_check.Checked = true;
                        monStart.Value = DateTime.Parse(time.start.ToString());
                        monEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        monday_check.Checked = false;
                        monStart.Value = DateTime.Parse("00:00:00");
                        monEnd.Value = DateTime.Parse("00:00:00");
                    } break;
                case "Вторник": if (time != null)
                    {
                        tuesday_check.Checked = true;
                        tueStart.Value = DateTime.Parse(time.start.ToString());
                        tueEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        tuesday_check.Checked = false;
                        tueStart.Value = DateTime.Parse("00:00:00");
                        tueEnd.Value = DateTime.Parse("00:00:00");
                    } break;
                case "Среда": if (time != null)
                    {
                        wednesday_check.Checked = true;
                        wenStart.Value = DateTime.Parse(time.start.ToString());
                        wenEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        wednesday_check.Checked = false;
                        wenStart.Value = DateTime.Parse("00:00:00");
                        wenEnd.Value = DateTime.Parse("00:00:00");
                    } break;
                case "Четверг": if (time != null)
                    {
                        thursday_check.Checked = true;
                        thuStart.Value = DateTime.Parse(time.start.ToString());
                        thuEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        thursday_check.Checked = false;
                        thuStart.Value = DateTime.Parse("00:00:00");
                        thuEnd.Value = DateTime.Parse("00:00:00");
                    } break;
                case "Пятница": if (time != null)
                    {
                        friday_check.Checked = true;
                        friStart.Value = DateTime.Parse(time.start.ToString());
                        friEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        friStart.Value = DateTime.Parse("00:00:00");
                        friEnd.Value = DateTime.Parse("00:00:00");
                        friday_check.Checked = false;
                    } break;
                case "Суббота": if (time != null)
                    {
                        saturday_check.Checked = true;
                        satStart.Value = DateTime.Parse(time.start.ToString());
                        satEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        satStart.Value = DateTime.Parse("00:00:00");
                        satEnd.Value = DateTime.Parse("00:00:00");
                        saturday_check.Checked = false;
                    } break;
                case "Воскресение": if (time != null)
                    {
                        sunday_check.Checked = true;
                        sunStart.Value = DateTime.Parse(time.start.ToString());
                        sunEnd.Value = DateTime.Parse(time.end.ToString());
                    }
                    else
                    {
                        sunday_check.Checked = false;
                        sunStart.Value = DateTime.Parse("00:00:00");
                        sunEnd.Value = DateTime.Parse("00:00:00");
                    } break;
            }
        }

        //При вкл. откл. боксов включаем или выключаем пикеры времени
        private void monday_check_CheckedChanged(object sender, EventArgs e)
        {
            monEnd.Enabled = monStart.Enabled = monday_check.Checked;
        }

        private void tuesday_check_CheckedChanged(object sender, EventArgs e)
        {
            tueEnd.Enabled = tueStart.Enabled = tuesday_check.Checked;
        }

        private void wednesday_check_CheckedChanged(object sender, EventArgs e)
        {
            wenEnd.Enabled = wenStart.Enabled = wednesday_check.Checked;
        }

        private void thursday_check_CheckedChanged(object sender, EventArgs e)
        {
            thuEnd.Enabled = thuStart.Enabled = thursday_check.Checked;
        }

        private void friday_check_CheckedChanged(object sender, EventArgs e)
        {
            friEnd.Enabled = friStart.Enabled = friday_check.Checked;
        }

        private void saturday_check_CheckedChanged(object sender, EventArgs e)
        {
            satEnd.Enabled = satStart.Enabled = saturday_check.Checked;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            sunEnd.Enabled = sunStart.Enabled = sunday_check.Checked;
        }

        //Кнопка сохр расписание. 
        private void button2_Click(object sender, EventArgs e)
        {

            //Для каждой строки получаем времена, которые хранятся по колонкам и сохраняем все в  БД
            if (schedule_dgv.SelectedRows.Count > 0)
            {
                var item = (schedule_dgv.SelectedRows[0].DataBoundItem as DataRowView).Row;

                for (int i = 2; i < item.Table.Columns.Count; i++)
                {
                    switch (item.Table.Columns[i].ColumnName)
                    {
                        case "Понедельник":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, monday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(monStart.Value.Hour, monStart.Value.Minute, 0),
                                end = new TimeSpan(monEnd.Value.Hour, monStart.Value.Minute, 0)
                            });
                            break;
                        case "Вторник":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, tuesday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(tueStart.Value.Hour, tueStart.Value.Minute, 0),
                                end = new TimeSpan(tueEnd.Value.Hour, tueEnd.Value.Minute, 0)
                            });
                            break;
                        case "Среда":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, wednesday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(wenStart.Value.Hour, wenStart.Value.Minute, 0),
                                end = new TimeSpan(wenEnd.Value.Hour, wenEnd.Value.Minute, 0)
                            });
                            break;
                        case "Четверг":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, thursday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(thuStart.Value.Hour, thuStart.Value.Minute, 0),
                                end = new TimeSpan(thuEnd.Value.Hour, thuEnd.Value.Minute, 0)
                            });
                            break;
                        case "Пятница":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, friday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(friStart.Value.Hour, friStart.Value.Minute, 0),
                                end = new TimeSpan(friEnd.Value.Hour, friEnd.Value.Minute, 0)
                            });
                            break;
                        case "Суббота":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, saturday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(satStart.Value.Hour, satStart.Value.Minute, 0),
                                end = new TimeSpan(satEnd.Value.Hour, satEnd.Value.Minute, 0)
                            });
                            break;
                        case "Воскресение":
                            ChangeScheduleFromDay((item[0] as Employee), item.Table.Columns[i].ColumnName, sunday_check.Checked, new ScheduleTime()
                            {
                                start = new TimeSpan(sunStart.Value.Hour, sunStart.Value.Minute, 0),
                                end = new TimeSpan(sunEnd.Value.Hour, sunEnd.Value.Minute, 0)
                            });
                            break;
                    }
                }
                schedule_dgv.DataSource = ScheduleBuilder.Scedule;
            }
        }

        //сохранить 1 день для сотрудника
        void ChangeScheduleFromDay(Employee empl, string DayOfWeak, bool Checked, ScheduleTime time)
        {
            if (Checked)
            {
                DataAccess.ChangeSchedule(empl, DayOfWeak, time);
            }
            else
            {
                try
                {
                    DataAccess.DeleteScheduleDay(empl.ID_Employee, DayOfWeak);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        //меняем времена конца в зависимости от время начала, чтобы не получилось, что время окончания работы менше времени начала
        private void monStart_ValueChanged(object sender, EventArgs e)
        {
            monEnd.Value = monStart.Value.AddHours(1);
        }

        private void tueStart_ValueChanged(object sender, EventArgs e)
        {
            tueEnd.Value = tueStart.Value.AddHours(1);
        }

        private void wenStart_ValueChanged(object sender, EventArgs e)
        {
            wenEnd.Value = wenStart.Value.AddHours(1);

        }

        private void thuStart_ValueChanged(object sender, EventArgs e)
        {
            thuEnd.Value = thuStart.Value.AddHours(1);

        }

        private void friStart_ValueChanged(object sender, EventArgs e)
        {
            friEnd.Value = friStart.Value.AddHours(1);

        }

        private void satStart_ValueChanged(object sender, EventArgs e)
        {
            satEnd.Value = tueStart.Value.AddHours(1);

        }

        private void sunStart_ValueChanged(object sender, EventArgs e)
        {
            sunEnd.Value = sunStart.Value.AddHours(1);
        }

        private void печатьРасписанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var reportTable = DataAccess.GetScheduleTimeTable();
            foreach (var item in ScheduleBuilder.Scedule.AsEnumerable())
            {
                var row = reportTable.NewRow();
                for (int i = 0; i < reportTable.Columns.Count; i++)
                {
                    row[i] = item[i];
                }
                reportTable.Rows.Add(row);
            }

            reportForm rf = new reportForm("ReportSchedule", reportTable);
            rf.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
