﻿namespace SportClub_Client.Views
{
    partial class ScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.schedule_dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.monday_check = new System.Windows.Forms.CheckBox();
            this.tuesday_check = new System.Windows.Forms.CheckBox();
            this.wednesday_check = new System.Windows.Forms.CheckBox();
            this.thursday_check = new System.Windows.Forms.CheckBox();
            this.friday_check = new System.Windows.Forms.CheckBox();
            this.saturday_check = new System.Windows.Forms.CheckBox();
            this.sunday_check = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.sunEnd = new System.Windows.Forms.DateTimePicker();
            this.sunStart = new System.Windows.Forms.DateTimePicker();
            this.satEnd = new System.Windows.Forms.DateTimePicker();
            this.satStart = new System.Windows.Forms.DateTimePicker();
            this.friEnd = new System.Windows.Forms.DateTimePicker();
            this.friStart = new System.Windows.Forms.DateTimePicker();
            this.thuEnd = new System.Windows.Forms.DateTimePicker();
            this.thuStart = new System.Windows.Forms.DateTimePicker();
            this.lbl = new System.Windows.Forms.Label();
            this.wenEnd = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.wenStart = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.tueEnd = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.tueStart = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.monEnd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.monStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.hours_lbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedule_dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.78571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.21428F));
            this.tableLayoutPanel1.Controls.Add(this.button3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.schedule_dgv, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 94.90085F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.09915F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 729);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Image = global::SportClub_Client.Properties.Resources.New_Window_24;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(777, 694);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(228, 32);
            this.button3.TabIndex = 23;
            this.button3.Text = "Выход";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // schedule_dgv
            // 
            this.schedule_dgv.AllowUserToAddRows = false;
            this.schedule_dgv.AllowUserToDeleteRows = false;
            this.schedule_dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory;
            this.schedule_dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.schedule_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.schedule_dgv.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.schedule_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.schedule_dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.schedule_dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedule_dgv.Location = new System.Drawing.Point(3, 3);
            this.schedule_dgv.MultiSelect = false;
            this.schedule_dgv.Name = "schedule_dgv";
            this.schedule_dgv.ReadOnly = true;
            this.schedule_dgv.RowHeadersVisible = false;
            this.schedule_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.schedule_dgv.Size = new System.Drawing.Size(768, 685);
            this.schedule_dgv.TabIndex = 0;
            this.schedule_dgv.SelectionChanged += new System.EventHandler(this.schedule_dgv_SelectionChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.monday_check);
            this.panel1.Controls.Add(this.tuesday_check);
            this.panel1.Controls.Add(this.wednesday_check);
            this.panel1.Controls.Add(this.thursday_check);
            this.panel1.Controls.Add(this.friday_check);
            this.panel1.Controls.Add(this.saturday_check);
            this.panel1.Controls.Add(this.sunday_check);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.sunEnd);
            this.panel1.Controls.Add(this.sunStart);
            this.panel1.Controls.Add(this.satEnd);
            this.panel1.Controls.Add(this.satStart);
            this.panel1.Controls.Add(this.friEnd);
            this.panel1.Controls.Add(this.friStart);
            this.panel1.Controls.Add(this.thuEnd);
            this.panel1.Controls.Add(this.thuStart);
            this.panel1.Controls.Add(this.lbl);
            this.panel1.Controls.Add(this.wenEnd);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.wenStart);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tueEnd);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tueStart);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.monEnd);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.monStart);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(777, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 685);
            this.panel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(8, 372);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(209, 34);
            this.button1.TabIndex = 1000;
            this.button1.Text = "Печать расписания";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.печатьРасписанияToolStripMenuItem_Click);
            // 
            // monday_check
            // 
            this.monday_check.AutoSize = true;
            this.monday_check.Checked = true;
            this.monday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.monday_check.Location = new System.Drawing.Point(64, 15);
            this.monday_check.Name = "monday_check";
            this.monday_check.Size = new System.Drawing.Size(15, 14);
            this.monday_check.TabIndex = 1;
            this.monday_check.UseVisualStyleBackColor = true;
            this.monday_check.CheckedChanged += new System.EventHandler(this.monday_check_CheckedChanged);
            // 
            // tuesday_check
            // 
            this.tuesday_check.AutoSize = true;
            this.tuesday_check.Checked = true;
            this.tuesday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tuesday_check.Location = new System.Drawing.Point(64, 58);
            this.tuesday_check.Name = "tuesday_check";
            this.tuesday_check.Size = new System.Drawing.Size(15, 14);
            this.tuesday_check.TabIndex = 4;
            this.tuesday_check.UseVisualStyleBackColor = true;
            this.tuesday_check.CheckedChanged += new System.EventHandler(this.tuesday_check_CheckedChanged);
            // 
            // wednesday_check
            // 
            this.wednesday_check.AutoSize = true;
            this.wednesday_check.Checked = true;
            this.wednesday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wednesday_check.Location = new System.Drawing.Point(64, 99);
            this.wednesday_check.Name = "wednesday_check";
            this.wednesday_check.Size = new System.Drawing.Size(15, 14);
            this.wednesday_check.TabIndex = 7;
            this.wednesday_check.UseVisualStyleBackColor = true;
            this.wednesday_check.CheckedChanged += new System.EventHandler(this.wednesday_check_CheckedChanged);
            // 
            // thursday_check
            // 
            this.thursday_check.AutoSize = true;
            this.thursday_check.Checked = true;
            this.thursday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.thursday_check.Location = new System.Drawing.Point(64, 142);
            this.thursday_check.Name = "thursday_check";
            this.thursday_check.Size = new System.Drawing.Size(15, 14);
            this.thursday_check.TabIndex = 10;
            this.thursday_check.UseVisualStyleBackColor = true;
            this.thursday_check.CheckedChanged += new System.EventHandler(this.thursday_check_CheckedChanged);
            // 
            // friday_check
            // 
            this.friday_check.AutoSize = true;
            this.friday_check.Checked = true;
            this.friday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.friday_check.Location = new System.Drawing.Point(64, 186);
            this.friday_check.Name = "friday_check";
            this.friday_check.Size = new System.Drawing.Size(15, 14);
            this.friday_check.TabIndex = 13;
            this.friday_check.UseVisualStyleBackColor = true;
            this.friday_check.CheckedChanged += new System.EventHandler(this.friday_check_CheckedChanged);
            // 
            // saturday_check
            // 
            this.saturday_check.AutoSize = true;
            this.saturday_check.Checked = true;
            this.saturday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saturday_check.Location = new System.Drawing.Point(64, 232);
            this.saturday_check.Name = "saturday_check";
            this.saturday_check.Size = new System.Drawing.Size(15, 14);
            this.saturday_check.TabIndex = 16;
            this.saturday_check.UseVisualStyleBackColor = true;
            this.saturday_check.CheckedChanged += new System.EventHandler(this.saturday_check_CheckedChanged);
            // 
            // sunday_check
            // 
            this.sunday_check.AutoSize = true;
            this.sunday_check.Checked = true;
            this.sunday_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sunday_check.Location = new System.Drawing.Point(64, 281);
            this.sunday_check.Name = "sunday_check";
            this.sunday_check.Size = new System.Drawing.Size(15, 14);
            this.sunday_check.TabIndex = 19;
            this.sunday_check.UseVisualStyleBackColor = true;
            this.sunday_check.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Image = global::SportClub_Client.Properties.Resources.OK_24;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(8, 334);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(209, 32);
            this.button2.TabIndex = 22;
            this.button2.Text = "Сохранить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // sunEnd
            // 
            this.sunEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sunEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.sunEnd.Location = new System.Drawing.Point(126, 297);
            this.sunEnd.Name = "sunEnd";
            this.sunEnd.ShowUpDown = true;
            this.sunEnd.Size = new System.Drawing.Size(92, 20);
            this.sunEnd.TabIndex = 21;
            // 
            // sunStart
            // 
            this.sunStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sunStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.sunStart.Location = new System.Drawing.Point(8, 297);
            this.sunStart.Name = "sunStart";
            this.sunStart.ShowUpDown = true;
            this.sunStart.Size = new System.Drawing.Size(98, 20);
            this.sunStart.TabIndex = 20;
            this.sunStart.ValueChanged += new System.EventHandler(this.sunStart_ValueChanged);
            // 
            // satEnd
            // 
            this.satEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.satEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.satEnd.Location = new System.Drawing.Point(126, 248);
            this.satEnd.Name = "satEnd";
            this.satEnd.ShowUpDown = true;
            this.satEnd.Size = new System.Drawing.Size(92, 20);
            this.satEnd.TabIndex = 18;
            // 
            // satStart
            // 
            this.satStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.satStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.satStart.Location = new System.Drawing.Point(8, 248);
            this.satStart.Name = "satStart";
            this.satStart.ShowUpDown = true;
            this.satStart.Size = new System.Drawing.Size(98, 20);
            this.satStart.TabIndex = 17;
            this.satStart.ValueChanged += new System.EventHandler(this.satStart_ValueChanged);
            // 
            // friEnd
            // 
            this.friEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.friEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.friEnd.Location = new System.Drawing.Point(126, 202);
            this.friEnd.Name = "friEnd";
            this.friEnd.ShowUpDown = true;
            this.friEnd.Size = new System.Drawing.Size(92, 20);
            this.friEnd.TabIndex = 15;
            // 
            // friStart
            // 
            this.friStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.friStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.friStart.Location = new System.Drawing.Point(8, 202);
            this.friStart.Name = "friStart";
            this.friStart.ShowUpDown = true;
            this.friStart.Size = new System.Drawing.Size(98, 20);
            this.friStart.TabIndex = 14;
            this.friStart.ValueChanged += new System.EventHandler(this.friStart_ValueChanged);
            // 
            // thuEnd
            // 
            this.thuEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.thuEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.thuEnd.Location = new System.Drawing.Point(126, 158);
            this.thuEnd.Name = "thuEnd";
            this.thuEnd.ShowUpDown = true;
            this.thuEnd.Size = new System.Drawing.Size(92, 20);
            this.thuEnd.TabIndex = 12;
            // 
            // thuStart
            // 
            this.thuStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thuStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.thuStart.Location = new System.Drawing.Point(8, 158);
            this.thuStart.Name = "thuStart";
            this.thuStart.ShowUpDown = true;
            this.thuStart.Size = new System.Drawing.Size(98, 20);
            this.thuStart.TabIndex = 11;
            this.thuStart.ValueChanged += new System.EventHandler(this.thuStart_ValueChanged);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(78, 281);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(74, 13);
            this.lbl.TabIndex = 999;
            this.lbl.Text = "Воскресенье";
            // 
            // wenEnd
            // 
            this.wenEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.wenEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.wenEnd.Location = new System.Drawing.Point(126, 115);
            this.wenEnd.Name = "wenEnd";
            this.wenEnd.ShowUpDown = true;
            this.wenEnd.Size = new System.Drawing.Size(92, 20);
            this.wenEnd.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(91, 232);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 999;
            this.label6.Text = "Суббота";
            // 
            // wenStart
            // 
            this.wenStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wenStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.wenStart.Location = new System.Drawing.Point(8, 115);
            this.wenStart.Name = "wenStart";
            this.wenStart.ShowUpDown = true;
            this.wenStart.Size = new System.Drawing.Size(98, 20);
            this.wenStart.TabIndex = 8;
            this.wenStart.ValueChanged += new System.EventHandler(this.wenStart_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(90, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 999;
            this.label5.Text = "Пятница";
            // 
            // tueEnd
            // 
            this.tueEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tueEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.tueEnd.Location = new System.Drawing.Point(126, 74);
            this.tueEnd.Name = "tueEnd";
            this.tueEnd.ShowUpDown = true;
            this.tueEnd.Size = new System.Drawing.Size(92, 20);
            this.tueEnd.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 999;
            this.label4.Text = "Четверг";
            // 
            // tueStart
            // 
            this.tueStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tueStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.tueStart.Location = new System.Drawing.Point(8, 74);
            this.tueStart.Name = "tueStart";
            this.tueStart.ShowUpDown = true;
            this.tueStart.Size = new System.Drawing.Size(98, 20);
            this.tueStart.TabIndex = 5;
            this.tueStart.ValueChanged += new System.EventHandler(this.tueStart_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(96, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 999;
            this.label3.Text = "Среда";
            // 
            // monEnd
            // 
            this.monEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.monEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.monEnd.Location = new System.Drawing.Point(126, 31);
            this.monEnd.Name = "monEnd";
            this.monEnd.ShowUpDown = true;
            this.monEnd.Size = new System.Drawing.Size(92, 20);
            this.monEnd.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(91, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 999;
            this.label2.Text = "Вторник";
            // 
            // monStart
            // 
            this.monStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.monStart.Location = new System.Drawing.Point(8, 31);
            this.monStart.Name = "monStart";
            this.monStart.ShowUpDown = true;
            this.monStart.Size = new System.Drawing.Size(98, 20);
            this.monStart.TabIndex = 2;
            this.monStart.ValueChanged += new System.EventHandler(this.monStart_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 999;
            this.label1.Text = "Понедельник";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.hours_lbl);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 694);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(768, 32);
            this.panel2.TabIndex = 24;
            // 
            // hours_lbl
            // 
            this.hours_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.hours_lbl.AutoSize = true;
            this.hours_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hours_lbl.Location = new System.Drawing.Point(140, 9);
            this.hours_lbl.Name = "hours_lbl";
            this.hours_lbl.Size = new System.Drawing.Size(65, 13);
            this.hours_lbl.TabIndex = 26;
            this.hours_lbl.Text = "<HOURS>";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Всего часов за неделю:";
            // 
            // ScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "ScheduleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Расписание сотрудников";
            this.Load += new System.EventHandler(this.ScheduleForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.schedule_dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView schedule_dgv;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox monday_check;
        private System.Windows.Forms.CheckBox tuesday_check;
        private System.Windows.Forms.CheckBox wednesday_check;
        private System.Windows.Forms.CheckBox thursday_check;
        private System.Windows.Forms.CheckBox friday_check;
        private System.Windows.Forms.CheckBox saturday_check;
        private System.Windows.Forms.CheckBox sunday_check;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker sunEnd;
        private System.Windows.Forms.DateTimePicker sunStart;
        private System.Windows.Forms.DateTimePicker satEnd;
        private System.Windows.Forms.DateTimePicker satStart;
        private System.Windows.Forms.DateTimePicker friEnd;
        private System.Windows.Forms.DateTimePicker friStart;
        private System.Windows.Forms.DateTimePicker thuEnd;
        private System.Windows.Forms.DateTimePicker thuStart;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.DateTimePicker wenEnd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker wenStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker tueEnd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker tueStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker monEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker monStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label hours_lbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}