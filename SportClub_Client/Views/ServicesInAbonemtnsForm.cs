﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SportClub_Client.Data;

namespace SportClub_Client.Views
{
    public partial class ServicesInAbonemtnsForm : Form
    {
        public ServicesInAbonemtnsForm()
        {
            InitializeComponent();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
           this.Close();
        }

        private void ServicesInAbonemtnsForm_Load(object sender, EventArgs e)
        {
            service_cmbx.DataSource = DataAccess.GetServices();
            dgv.DataSource = DataAccess.GetAbonements();
        }

        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Title"] != null)
                dgv.Columns["Title"].HeaderText = "Название абонемента";

            if (dgv.Columns["isUnlimit"] != null)
                dgv.Columns["isUnlimit"].Visible = false;

            if (dgv.Columns["ID_Abonement"] != null)
                dgv.Columns["ID_Abonement"].Visible = false;
            if (dgv.Columns["Price"] != null)
                dgv.Columns["Price"].Visible = false;
            if (dgv.Columns["ServicesAbonement"] != null)
                dgv.Columns["ServicesAbonement"].Visible = false;
            if (dgv.Columns["AbonemetsClient"] != null)
                dgv.Columns["AbonemetsClient"].Visible = false;
            if (dgv.Columns["CountMonth"] != null)
                dgv.Columns["CountMonth"].Visible = false;
            if (dgv.Columns["TimeStart"] != null)
                dgv.Columns["TimeStart"].Visible = false;
            if (dgv.Columns["TimeEnd"] != null)
                dgv.Columns["TimeEnd"].Visible = false;
        }

        private void service_dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (service_dgv.Columns["Service"] != null)
                service_dgv.Columns["Service"].HeaderText = "Название услуги"; 
            if (service_dgv.Columns["Count"] != null)
                service_dgv.Columns["Count"].HeaderText = "Количество в абонементе";
            if (service_dgv.Columns["Abonement"] != null)
                service_dgv.Columns["Abonement"].Visible = false;
            if (service_dgv.Columns["Abonement_ID"] != null)
                service_dgv.Columns["Abonement_ID"].Visible = false;
            if (service_dgv.Columns["Service_ID"] != null)
                service_dgv.Columns["Service_ID"].Visible = false;
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count>0)
            {
                var item = dgv.SelectedRows[0].DataBoundItem as Abonement;
                service_dgv.DataSource = DataAccess.GetServicesINAbon(item);
                
            }
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {

            if (dgv.SelectedRows.Count > 0)
            {
                var item = dgv.SelectedRows[0].DataBoundItem as Abonement;

                if ((service_cmbx.SelectedItem as Service)!=null)
                {
                    var serv = service_cmbx.SelectedItem as Service;
                    DataAccess.AddServiceToAbon(item, serv, (int)numericUpDown1.Value);

                    service_dgv.DataSource = DataAccess.GetServicesINAbon(item);
                }
            }
        }
    }
}
