﻿using SportClub_Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class AbonementForm : Form
    {
        public AbonementForm()
        {
            InitializeComponent();
        }

        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            txbx.Text = string.Empty; //Обнуляем то что в них было
            timestart_dtpc.Value = new DateTime(2000, 1, 1, 0, 0, 0);
            timeend_dtpc.Value = new DateTime(2000, 1, 1, 0, 0, 0);
            count_num.Value = 0;
            price_num.Value = 0;
            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = txbx.Enabled = count_num.Enabled = price_num.Enabled = timeend_dtpc.Enabled = timestart_dtpc.Enabled = unlim_chbx.Enabled = false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = txbx.Enabled = count_num.Enabled = price_num.Enabled = timeend_dtpc.Enabled = timestart_dtpc.Enabled = unlim_chbx.Enabled = true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetAbonements();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            //Если включен режим редактирования
            if (isEdit)
            {
                //Выбрана ли страна

                //Проверяем, выбран ли элемент для редактирования
                if (dgv.SelectedRows.Count > 0)
                {
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as Abonement;
                    item.Title = txbx.Text; //заполняем его данными из текстбокса
                    item.TimeStart = timestart_dtpc.Value;
                    item.TimeEnd = timeend_dtpc.Value;
                    item.isUnlimit = unlim_chbx.Checked;
                    item.CountMonth = (int)count_num.Value;
                    item.Price = price_num.Value;
                    //Обновляем данные в БД
                    if (timeend_dtpc.Value.Hour > timestart_dtpc.Value.Hour)
                    {
                        Data.DataAccess.UpdateAbonemet(item);
                    }
                    else MessageBox.Show("Время посещений указано не верно");

                }
            }
            else
            {
                //Создаем новый объект 
                var item = new Abonement();
                //Заполняем его
                item.Title = txbx.Text; //заполняем его данными из текстбокса
                item.TimeStart = timestart_dtpc.Value;
                item.TimeEnd = timeend_dtpc.Value;
                item.isUnlimit = unlim_chbx.Checked;
                item.CountMonth = (int)count_num.Value;
                item.Price = price_num.Value;
                //Вставляем запись в БД
                Data.DataAccess.InsertAbonemet(item);
            }
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetAbonements();
        }

        //Событие возникает после привязки данных, т.е после изменения dgv.DataSource
        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Title"] != null)
                dgv.Columns["Title"].HeaderText = "Название абонемента";

            if (dgv.Columns["isUnlimit"] != null)
                dgv.Columns["isUnlimit"].Visible = false;

            if (dgv.Columns["ID_Abonement"] != null)
                dgv.Columns["ID_Abonement"].Visible = false;
            if (dgv.Columns["Price"] != null)
                dgv.Columns["Price"].Visible = false;
            if (dgv.Columns["ServicesAbonement"] != null)
                dgv.Columns["ServicesAbonement"].Visible = false;
            if (dgv.Columns["AbonemetsClient"] != null)
                dgv.Columns["AbonemetsClient"].Visible = false;
            if (dgv.Columns["CountMonth"] != null)
                dgv.Columns["CountMonth"].Visible = false;
            if (dgv.Columns["TimeStart"] != null)
                dgv.Columns["TimeStart"].Visible = false;
            if (dgv.Columns["TimeEnd"] != null)
                dgv.Columns["TimeEnd"].Visible = false;
        }

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as Abonement;

                //Выводим в текстбокс
                txbx.Text = item.Title;
                timestart_dtpc.Value = item.TimeStart;
                timeend_dtpc.Value = item.TimeEnd;
                count_num.Value = item.CountMonth;
                price_num.Value = item.Price;
                unlim_chbx.Checked = item.isUnlimit;

                DisableControls();
                isEdit = false;
            }
        }

        //Событие загрузки формы
        private void Form_Load(object sender, EventArgs e)
        {
            //Заполняем таблицу
            dgv.DataSource = DataAccess.GetAbonements();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LecturerForm_Load(object sender, EventArgs e)
        {
            dgv.DataSource = DataAccess.GetAbonements();
        }

        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Abonement;
                DataAccess.DeleteAbonement(item);

                dgv.DataSource = DataAccess.GetAbonements();
            }
        }

        private void обновитьДанныкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = DataAccess.GetAbonements();
        }

        private void unlim_chbx_CheckedChanged(object sender, EventArgs e)
        {
            if (unlim_chbx.Checked)
            {
                timestart_dtpc.Value = new DateTime(2000, 1, 1, 0, 0, 0);
                timeend_dtpc.Value = new DateTime(2000, 1, 1, 23, 59, 59);
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Abonement;
                DataAccess.DeleteAbonement(item);

                dgv.DataSource = DataAccess.GetAbonements();
            }
        }
    }
}
