﻿using SportClub_Client.Data;
using System;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class ServiceForm : Form
    {
        public ServiceForm()
        {
            InitializeComponent();
        }

        public bool isEdit { get; set; }


        //Кнопка добавить запись
        private void add_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            txbx.Text = string.Empty; //Обнуляем то что в них было
            info_txbx.Text = string.Empty;
            price_num.Value = 0;
            isEdit = false; //Ставим флаг в false, что сейчас идет не редактирование
            edit_btn.Enabled = false; //Выключаем кнопку редактирования
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            EnableControls(); //Включаем контролы для редактирования
            isEdit = true; //Ставим флаг, что сейчас идет редактирование
            add_btn.Enabled = false; //Выключаем кнопку добавить
        }

        //Выключаемконтролы для редактирования
        void DisableControls()
        {
            save_btn.Enabled = txbx.Enabled = info_txbx.Enabled = price_num.Enabled= false;
            add_btn.Enabled = edit_btn.Enabled = true;
        }
        //Включаем их
        void EnableControls()
        {
            save_btn.Enabled = txbx.Enabled  = info_txbx.Enabled = price_num.Enabled =true;
        }



        //Метод (обработчик событий) для обновления данных в датагриде
        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = Data.DataAccess.GetServices();
        }

        //Обработчик нажатия кнопки сохранить
        private void save_btn_Click(object sender, EventArgs e)
        {
            //Если включен режим редактирования
            if (isEdit)
            {
            
                //Проверяем, выбран ли элемент для редактирования
                if (dgv.SelectedRows.Count > 0)
                {
                    //Получаем элемент из выбранной строки
                    var item = dgv.SelectedRows[0].DataBoundItem as Service;
                    item.Title = txbx.Text; //заполняем его данными из текстбокса
                    item.Information = info_txbx.Text;
                    item.Price = price_num.Value;
                    Data.DataAccess.UpdateService(item);
                }
            }
            else
            {
                //Создаем новый объект 
                var item = new Service();
                //Заполняем его
                item.Title = txbx.Text; //заполняем его данными из текстбокса
                item.Information = info_txbx.Text;
                item.Price = price_num.Value;
                //Вставляем запись в БД
                Data.DataAccess.InsertService(item);
            }
            //Выключаем необходимые элементы управления
            DisableControls();
            //Обновляем данные в датагриде
            dgv.DataSource = Data.DataAccess.GetServices();
        }

        //Событие возникает после привязки данных, т.е после изменения dgv.DataSource
        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Переименовываем колонки
            //Проверяем, если есть такая колонка
            if (dgv.Columns["Title"] != null)
                dgv.Columns["Title"].HeaderText = "Название услуги";

            if (dgv.Columns["isUnlimit"] != null)
                dgv.Columns["isUnlimit"].Visible = false;

            if (dgv.Columns["ID_Service"] != null)
                dgv.Columns["ID_Service"].Visible = false;
            if (dgv.Columns["ServicesAbonement"] != null)
                dgv.Columns["ServicesAbonement"].Visible = false;
            if (dgv.Columns["JournalServices"] != null)
                dgv.Columns["JournalServices"].Visible = false;
            if (dgv.Columns["Information"] != null)
                dgv.Columns["Information"].Visible = false;
            if (dgv.Columns["Price"] != null)
                dgv.Columns["Price"].Visible = false;
        }

        //Событие переключения строк в датагриде
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            //Если строка выбрана
            if (dgv.SelectedRows.Count > 0)
            {
                //Приводим строку из датагрида к объекту 
                var item = dgv.SelectedRows[0].DataBoundItem as Service;

                //Выводим в текстбокс
                txbx.Text = item.Title;
                info_txbx.Text = item.Information;
                price_num.Value = item.Price;

                DisableControls();
                isEdit = false;
            }
        }

        //Событие загрузки формы
        private void Form_Load(object sender, EventArgs e)
        {
            //Заполняем таблицу
            dgv.DataSource = DataAccess.GetServices();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LecturerForm_Load(object sender, EventArgs e)
        {
            dgv.DataSource = DataAccess.GetServices();
        }

        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                //Получаем элемент из выбранной строки
                var item = dgv.SelectedRows[0].DataBoundItem as Service;
                DataAccess.DeleteService(item);

                dgv.DataSource = DataAccess.GetServices();
            }
        }

        private void обновитьДанныкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgv.DataSource = null;
            dgv.DataSource = DataAccess.GetServices();
        }


    }
}
