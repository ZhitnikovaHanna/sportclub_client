﻿using SportClub_Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class reportForm : Form
    {
        string reportname;
       
        private DateTime dateTime1;
        private DateTime dateTime2;

        private DataTable reportTable;


        //3 конструктор для разных случаев отчета. Для только названия, для названия и периода, для названия и источника данных
        public reportForm(string reportname)
        {
            InitializeComponent();
            this.reportname = reportname;
        }

        public reportForm(string reportname, DateTime dateTime1, DateTime dateTime2)
        {
            InitializeComponent();
            this.reportname = reportname;
            this.dateTime1 = dateTime1;
            this.dateTime2 = dateTime2;
        }
        public reportForm(string ReportName, DataTable reportTable)
        {
            InitializeComponent();

            this.reportname = ReportName;
            this.reportTable = reportTable;
        }
        private void reportForm_Load(object sender, EventArgs e)
        {

            //в зависимости от названия отчета совершаем действия
            switch (reportname)
            {
                case "ServicesReport":
                    {

                        this.Text = "Прайс услуг спортивного клуба";
                        Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();//создеаем источник отчета
                        reportDataSource1.Name = "DataSet1";//называем
                        Data.DataSet1TableAdapters.ServiceTableAdapter repAdap = new Data.DataSet1TableAdapters.ServiceTableAdapter();
                        Data.DataSet1.ServiceDataTable repTable = new Data.DataSet1.ServiceDataTable();
                        repAdap.Fill(repTable);
                        reportDataSource1.Value = repTable;//источнику для репорта присваиваем таблицу 
                        this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1); // к нашему репорту добавляем источник
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SportClub_Client.Views.Report1.rdlc";
                    }
                    break;
                case "GymApparatusReport":
                    {

                        this.Text = "Инвентарь спортивного клуба";
                        Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();//создеаем источник отчета
                        reportDataSource1.Name = "DataSet1";//называем
                        var repAdap = new Data.DataSet1TableAdapters.GymApparatusReportTableAdapter();
                        var repTable = new Data.DataSet1.GymApparatusReportDataTable();
                        repAdap.Fill(repTable);
                        reportDataSource1.Value = repTable;//источнику для репорта присваиваем таблицу 
                        this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1); // к нашему репорту добавляем источник
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SportClub_Client.Views.Report6.rdlc";
                    }
                    break;
                case "AbonementsReport":
                    {
                        this.Text = "Aбонементы и клубные карты";
                        Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();//создеаем источник отчета
                        reportDataSource1.Name = "DataSet1";//называем
                        Data.DataSet1TableAdapters. AbonementReportTableAdapter repAdap = new Data.DataSet1TableAdapters. AbonementReportTableAdapter();
                        Data.DataSet1. AbonementReportDataTable repTable = new Data.DataSet1. AbonementReportDataTable();
                        repAdap.Fill(repTable);
                        reportDataSource1.Value = repTable;//источнику для репорта присваиваем таблицу 
                        this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1); // к нашему репорту добавляем источник
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SportClub_Client.Views.Report2.rdlc";
                    }
                    break;

                case "EmployeeReport":
                    {
                        this.Text = "Отчет по сотрудникам";
                        Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();//создеаем источник отчета
                        reportDataSource1.Name = "DataSet1";//называем
                        
                        reportDataSource1.Value = DataAccess.GetEmployeesReport(dateTime1, dateTime2);//источнику для репорта присваиваем таблицу 
                        this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1); // к нашему репорту добавляем источник
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SportClub_Client.Views.Report3.rdlc";
                    } break;

                case "ClientReport":
                    {
                        this.Text = "Отчет по клиентам";
                        Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();//создеаем источник отчета
                        reportDataSource1.Name = "DataSet1";//называем

                        reportDataSource1.Value = DataAccess.GetClientReport(dateTime1, dateTime2);//источнику для репорта присваиваем таблицу 
                        this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1); // к нашему репорту добавляем источник
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SportClub_Client.Views.Report4.rdlc";
                    } break;
                case "ReportSchedule":
                {
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();//создеаем источник отчета
                    reportDataSource1.Name = "DataSet1";//называем
                    reportDataSource1.Value = reportTable;//источнику для репорта присваеваем таблицу 
                    this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1); // к нашему репорту добавляем источник
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "SportClub_Client.Views.Report5.rdlc";
                }
                    break;
                default:
                    break;
            }
            this.reportViewer1.RefreshReport();
        }
    }
}
