﻿using SportClub_Client.Data;
using System;
using System.Windows.Forms;

namespace SportClub_Client.Views
{
    public partial class SelectTrenerForm : Form
    {
        public bool isOK;
        public Employee employee;

        public SelectTrenerForm()
        {
            InitializeComponent();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectTrenerForm_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = DataAccess.GetTreners();
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                employee = comboBox1.SelectedItem as Employee;
                isOK = true;
                this.Close();
            }
            else MessageBox.Show("Тренер не выбран!");
        }
    }
}
