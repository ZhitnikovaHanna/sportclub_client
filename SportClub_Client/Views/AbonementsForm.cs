﻿using System;
using System.Linq;
using System.Windows.Forms;
using SportClub_Client.Data;

namespace SportClub_Client.Views
{
    public partial class AbonementsForm : Form
    {
        //ссылка на абонемент
        private AbonemetsClient abon;

        //конструктор
        public AbonementsForm()
        {
            InitializeComponent();
            //очищаем все метки
            name_lbl.Text = abon_lbl.Text = status_lbl.Text = string.Empty;
        }
        //нажатие на кнопку поиска
        private void search_btn_Click(object sender, EventArgs e)
        {
            SearchAbon();
        }
        //изменить статус абонемента (заморозить)
        private void button2_Click(object sender, EventArgs e)
        {
            if (abon != null) //если абонемент есть
            {
                abon.Status = false; //замораживаем
                //сохр в БД
                DataAccess.ChangeStatusAbonement(abon);
                SearchAbon();
            }
        }

        //Тоже самое только наоборот, размораживаем
        private void button4_Click(object sender, EventArgs e)
        {
            if (abon != null)
            {
                abon.Status = true;
                DataAccess.ChangeStatusAbonement(abon);
                SearchAbon();
            }
        }
        //Поиск абонемента по ID
        //И вывод информ. в метки
        void SearchAbon()
        {
            int id;
            if (Int32.TryParse(textBox1.Text, out id))
            {
                abon = DataAccess.GetAbonementsClients(textBox1.Text).FirstOrDefault();

                if (abon != null)
                {
                    name_lbl.Text = abon.Client.Name;
                    abon_lbl.Text = abon.Abonement.Title;
                    dateTimePicker1.Value = abon.DateStart;
                    dateTimePicker2.Value = abon.DateEnd;
                    status_lbl.Text = abon.Status ? "Активирован" : "Заморожен";
                }
                else
                {
                    MessageBox.Show("Такой абонемент не найден!", "Не найден", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    name_lbl.Text = abon_lbl.Text = status_lbl.Text = string.Empty;
                    dateTimePicker1.Value = dateTimePicker2.Value = DateTime.Now.Date;
                }

            }
        }
        //продлить сроки действия абонемента
        private void button1_Click(object sender, EventArgs e)
        {
            if (abon != null)
            {
                //увеличиваем дату конца
                abon.DateEnd = dateTimePicker2.Value.Date;
                //сохр в БД
                DataAccess.ChangeStatusAbonement(abon);
                MessageBox.Show("Срок действия продлен", "Продлен", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                SearchAbon();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
