﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SportClub_Client
{
    //класс для шифрования паролей
    class SecurityClass
    {
        public static Guid GetSecurityPassword(string pass)
        {
            //Получаем двоичные данные каждого символа
            byte[] byteVal = Encoding.Unicode.GetBytes(pass);
            //создаем криптопровайдер - класс с помощью которого шифруем
            MD5CryptoServiceProvider csp = new MD5CryptoServiceProvider();

            //получаем массив байтов зашифрованных  спомощью криптопровайдера
            byte[] byteHash = csp.ComputeHash(byteVal);

            string res = string.Empty;
            foreach (byte b in byteHash)
                //формирем строку готового пароля
            {
                //берем каждый зашифрованный байт и превращаем его в шестнадцатеричный формат
                res += string.Format("{0:x2}", b);
            }
            //Получаем уникальный идентификатор для сформированной строки
            var s = new Guid(res).ToString();
            return new Guid(res); // возвращаем

            // теперь все пароли будут иметь одну длину в зашифрованном виде
        }
    }
}
