﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportClub_Client.Data
{
    //Класс для хранения данных о доступных (не использ.) услугах клиента (абонемента)
    class AviableServices
    {
        public string service { get; set; }
        public int count { get; set; }

        public int id_abonementclient;
        public int id_service;

    }
}
