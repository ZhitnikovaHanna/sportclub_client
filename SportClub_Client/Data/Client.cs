﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportClub_Client.Data
{
    //Часть класса с переопределенным методом ТоString() чтобы  отображался в визуальных элементах нормально
    partial class Client
    {
        public override string ToString()
        {
            return Name;
        }
    }
}
