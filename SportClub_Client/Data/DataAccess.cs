﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SportClub_Client.Data
{
    class DataAccess
    {
        //contex данных EntityFramework
        static SportClubDBEntities context;
        //ссылка на вошедшего в ПО пользователя
        public static Employee employee;
        static DataAccess()
        {
            //Создаем контект в статическом конструкторе, т.к. он отработает первый
            context = new SportClubDBEntities();
        }
        //Метод проверки пароля
        internal static bool CheckPassword(RoleEnum role, object selectedItem, string text)
        {
            switch (role)
            {
                case RoleEnum.Администратор:
                    return text == "admin123";

                case RoleEnum.Менеджер:
                    var manager = selectedItem as Employee;
                    return manager.Password == SecurityClass.GetSecurityPassword(text).ToString();

                default: return false;
            }
        }
        //Метод изменения расписания
        public static void ChangeSchedule(Employee empl, string DayOfWeak, ScheduleTime time)
        {
            //Выбираем элемент расписания
            var item = context.Schedule.Where(x => x.DayOfWeak == DayOfWeak && x.Employee.ID_Employee == empl.ID_Employee).FirstOrDefault();
            //если такого нет, создаем новый
            if (item == null)
            {
                Schedule schedule = new Schedule();
                schedule.TimeStart = (TimeSpan)time.start;
                schedule.TimeEnd = (TimeSpan)time.end;
                schedule.DayOfWeak = DayOfWeak;
                empl.Schedule.Add(schedule);
            }
            else //если есть меняем время работы
            {
                item.TimeStart = (TimeSpan)time.start;
                item.TimeEnd = (TimeSpan)time.end;
            }
            context.SaveChanges();
        }
        //Выбор тренеров (всех сотрудников)
        internal static object GetTreners()
        {
            return context.Employee.ToList();
        }
        //Выбор только Менеджеров
        internal static List<Employee> GetMenagers()
        {
            context.Post.ToList();
            return context.Employee.Where(x => x.Post.Title == "Менеджер").ToList();
        }

        //получить все абонементы
        internal static List<Abonement> GetAbonements()
        {
            return context.Abonement.ToList();
        }

        //Удалить абонемент
        internal static void DeleteAbonement(Abonement item)
        {
            context.DeleteObject(item);
            context.SaveChanges();
        }

        //обновить абонемент (сохранить изменения в контексте)
        internal static void UpdateAbonemet(Abonement item)
        {
            context.SaveChanges();
        }
        //Добавить абонемент клиенту
        internal static AbonemetsClient AddAbonementClient(Client client, Abonement item, DateTime date1, DateTime date2)
        {
            //создаем новый абонемент клиента и заполняем его
            AbonemetsClient ac = new AbonemetsClient()
            {
                Abonement = item,
                Client = client,
                DateStart = date1,
                DateEnd = date2,
                Employee_ID = employee.ID_Employee
            };
            //Добавляем в контекст, сохраняем
            context.AddToAbonemetsClient(ac);
            context.SaveChanges();
            //Возврааем посл. добавленный абонемент с теми параметрами что только что добавляли
            return context.AbonemetsClient.Where(x => x.Abonement.ID_Abonement == ac.Abonement.ID_Abonement &&
            client.ID_Client == ac.Client.ID_Client)
            .OrderByDescending(x => x.ID_AbonemetClient).FirstOrDefault();

        }
        //Вставка абонемента
        internal static void InsertAbonemet(Abonement item)
        {
            context.AddToAbonement(item);
            context.SaveChanges();
        }
        //получить список услуг
        internal static object GetServices()
        {
            return context.Service.ToList();
        }
        //обновить услугу
        internal static void UpdateService(Service item)
        {
            context.SaveChanges();
        }
        //получить скидки
        internal static List<DiscountRates> GetDiscounts()
        {
            return context.DiscountRates.ToList();
        }
        //удалить услугу
        internal static void DeleteService(Service item)
        {
            context.DeleteObject(item);
            context.SaveChanges();
        }
        //вставить услугу
        internal static void InsertService(Service item)
        {
            context.AddToService(item);
            context.SaveChanges();
        }
        //обновить данные клиента
        internal static void UpdateClient(Client item)
        {
            context.SaveChanges();
        }

        //добавить нового клиента
        internal static void InsertClient(Client item)
        {
            context.AddToClient(item);
            context.SaveChanges();
        }
        //Получить список клиентов
        internal static List<Client> GetClients()
        {
            return context.Client.ToList();
        }
        //обновить скидку
        internal static void UpdatDiscount(DiscountRates item)
        {
            context.SaveChanges();
        }
        //удалить клиента
        internal static void DeleteClient(Client item)
        {
            context.DeleteObject(item);
            context.SaveChanges();
        }
        //получить клиента по имени и телефону
        internal static List<Client> GetClients(string name, string phone)
        {
            var query = context.Client.AsEnumerable();//формируем запрос
            if (name.Length > 0) //если имя не пустое
            {
                //получаем всех клиентов у кого имя включает текст
                query = query.Where(x => x.Name.ToLower().Contains(name.ToLower().Trim()));
            }
            if (phone.Length > 0)
            {
                //получаем клиентов с определенным телефоном
                query = query.Where(x => x.Phone == phone);
            }

            return query.ToList(); //возвращаем
        }

        //вставка скидки
        internal static void InsertDiscount(DiscountRates item)
        {
            context.AddToDiscountRates(item);
            context.SaveChanges();
        }
        //обновить данные сотрудника
        internal static void UpdateEmployee(Employee item)
        {
            context.SaveChanges();
        }
        //вставка сотрудника
        internal static void InsertEmployee(Employee item)
        {
            context.AddToEmployee(item);
            context.SaveChanges();
        }
        //получить список сотр.
        internal static List<Employee> GetEmployees()
        {
            context.Schedule.ToList();
            return context.Employee.ToList();
        }
        //удалить сотрудника
        internal static void DeleteEmployee(Employee item)
        {
            context.DeleteObject(item);
            context.SaveChanges();
        }
        //получить список должностей
        internal static object GetPosts()
        {
            return context.Post.ToList();
        }
        //удалить скидку
        internal static void DeleteDiscount(DiscountRates item)
        {
            context.DeleteObject(item);
            context.SaveChanges();
        }
        //получить список услуг для определенного абонемента
        internal static List<ServicesAbonement> GetServicesINAbon(Abonement item)
        {
            return context.ServicesAbonement.Where(x => x.Abonement.ID_Abonement == item.ID_Abonement).ToList();
        }
        //добавить услугу к абонементу
        internal static void AddServiceToAbon(Abonement item, Service serv, int count)
        {
            //получаем выбранную услугу в абонементе
            var exist = context.ServicesAbonement
                .Where(x => x.Abonement_ID == item.ID_Abonement && x.Service_ID == serv.ID_Service).FirstOrDefault();
            //если такая есть, добавляем кол-во
            if (exist != null)
            {
                exist.Count += count;
            }
            else //если нет, то создаем новую услугу в этом абонементе
            {
                ServicesAbonement sa = new ServicesAbonement();
                sa.Service = serv;
                sa.Abonement = item;
                sa.Count = count;
                context.AddToServicesAbonement(sa);
            }
            context.SaveChanges();

        }
        //получить абонементы клиентов
        internal static List<AbonemetsClient> GetAbonementsClients()
        {
            context.Client.ToList();
            context.Abonement.ToList();
            return context.AbonemetsClient.ToList();
        }
        //получить абонемент клиента по номеру
        internal static List<AbonemetsClient> GetAbonementsClients(string num)
        {
            context.Client.ToList();
            context.Abonement.ToList();
            int id;
            if (int.TryParse(num, out id)) //если переданный номер явл. числом
            {
                //ищем абонемент и возвращаем
                return context.AbonemetsClient.Where(x => x.ID_AbonemetClient == id).ToList();
            }
            //иначе вернуть все абонементы
            else return context.AbonemetsClient.ToList();

        }
        //список доступных услуг для конкретного клиента (для его абонемента)
        internal static List<AviableServices> GetAviableService(AbonemetsClient item)
        {
            context.Service.ToList();
            context.ServicesAbonement.ToList();
            context.Abonement.ToList();

            return context.ServicesAbonement.Where(x => x.Abonement.ID_Abonement == item.Abonement.ID_Abonement).Select(x =>
                    new AviableServices()
                    {
                        service = x.Service.Title,
                        count = x.Count - x.Service.JournalServices
                        .Where(j => j.AbonemetsClient.Abonement.ID_Abonement == item.Abonement.ID_Abonement && j.Service.ID_Service == x.Service_ID).Count(),

                        id_abonementclient = item.ID_AbonemetClient,
                        id_service = x.Service_ID
                    }).ToList();
        }


        //Получить журнал посещений абонемента клиента
        internal static List<JournalServices> GetJournal(AbonemetsClient item)
        {
            return context.JournalServices.Where(x => x.AbonemetsClient.ID_AbonemetClient == item.ID_AbonemetClient).ToList();
        }

        //Получить журнал посещений абонемента клиента по ID абонемента
        internal static List<JournalServices> GetJournal(int item)
        {
            return context.JournalServices.Where(x => x.AbonemetsClient.ID_AbonemetClient == item).ToList();
        }

        //Использовать услугу из абонемента
        internal static void UseService(AviableServices item, Employee trener)
        {
            JournalServices js = new JournalServices();
            js.Cost = 0;
            js.DiscountClient = 0;
            js.Employee = employee;
            js.Employee1 = trener;
            js.Service = context.Service.Where(x => x.ID_Service == item.id_service).FirstOrDefault();
            js.AbonemetsClient = context.AbonemetsClient.Where(x => x.ID_AbonemetClient == item.id_abonementclient).FirstOrDefault();
            js.DateUse = DateTime.Now;
            context.AddToJournalServices(js);
            context.SaveChanges();
        }
        //Получить скидку для конкретного клиента
        internal static decimal GetDiscountClient(Client client)
        {
            context.Abonement.ToList();
            if (client.ID_Client != 1)
            {
                decimal amount = 0;
                //вычисляем сумму исп. его услуг и стоимости купленных абонементов
                amount += context.AbonemetsClient.Where(x => x.Client.ID_Client == client.ID_Client).Count() > 0 ?
                    context.AbonemetsClient.Where(x => x.Client.ID_Client == client.ID_Client).Sum(x => x.Abonement.Price) : 0;
                amount += context.JournalServices.Where(x => x.AbonemetsClient.Client.ID_Client == client.ID_Client).Count() > 0 ?
                    context.JournalServices.Where(x => x.AbonemetsClient.Client.ID_Client == client.ID_Client).Sum(x => x.Cost) : 0;
                //в зависимости от потраченной суммы, возвращаем дисконт
                return context.DiscountRates.Where(x => x.Amount <= amount).OrderByDescending(x => x.Amount).FirstOrDefault() != null ?
                    context.DiscountRates.Where(x => x.Amount <= amount).OrderByDescending(x => x.Amount).FirstOrDefault().Discount : 0;
            }
            else return 0;

        }
        //добавить запись в журнал посещений
        internal static void AddToJournal(Client cl, Service serv, decimal value, decimal disc, Employee trener)
        {
            JournalServices js = new JournalServices();
            js.Cost = value;
            js.DiscountClient = disc;
            js.Employee = employee;
            js.Employee1 = trener;
            js.Service = context.Service.Where(x => x.ID_Service == serv.ID_Service).FirstOrDefault();
            js.AbonemetsClient = context.AbonemetsClient.Where(x => x.ID_AbonemetClient == 2).FirstOrDefault();
            js.DateUse = DateTime.Now;
            context.AddToJournalServices(js);
            context.SaveChanges();
        }
        //вернуть таблицу с проведенными тренировками
        internal static DataSet1.EmpReportDataTable GetEmployeesReport(DateTime start, DateTime end)
        {
            DataSet1.EmpReportDataTable table = new DataSet1.EmpReportDataTable();
            foreach (var item in GetEmployees())//Получаем список сотр.
            {
                var row = table.NewEmpReportRow();//для каждого формируем строку

                row.Name = item.Name; // вычисляем кол-во проведенных тренеровок и проданных абонементов и суммы этих операций
                row.CountAbonements = context.AbonemetsClient.Where(x => x.Employee_ID == item.ID_Employee && x.DateStart >= start && x.DateStart <= end).Count().ToString();
                row.CountTren = context.JournalServices.Where(x => x.Employee1.ID_Employee == item.ID_Employee && x.DateUse >= start && x.DateUse <= end).Count().ToString();
                row.CountServicec = context.JournalServices.Where(x => x.Employee.ID_Employee == item.ID_Employee && x.DateUse >= start && x.DateUse <= end).Count().ToString();

                row.AmountAbon = row.CountAbonements == "0" ? "0" :
                    context.AbonemetsClient.Where(x => x.Employee_ID == item.ID_Employee && x.DateStart >= start && x.DateStart <= end).Sum(x => x.Abonement.Price).ToString();
                row.AmountServ = row.AmountServ == "0" ? "0" :
                    context.JournalServices.Where(x => x.Employee.ID_Employee == item.ID_Employee && x.DateUse >= start && x.DateUse <= end).Sum(x => x.Cost).ToString();

                table.Rows.Add(row);
            }

            return table;
        }

        //тоже самое для клиента
        internal static DataSet1.ClientReportDataTable GetClientReport(DateTime start, DateTime end)
        {
            context.AbonemetsClient.ToList();
            DataSet1.ClientReportDataTable table = new DataSet1.ClientReportDataTable();
            foreach (var item in GetClients())
            {
                var row = table.NewClientReportRow();

                row.Name = item.Name;
                row.CountAbonements = context.AbonemetsClient.Where(x => x.Client.ID_Client == item.ID_Client && x.DateStart >= start && x.DateStart <= end).Count().ToString();
                row.CountServ = context.JournalServices.Where(x => x.AbonemetsClient.Client.ID_Client == item.ID_Client && x.DateUse >= start && x.DateUse <= end).Count().ToString();

                row.AmountAbon = row.CountAbonements == "0" ? "0" :
                    context.AbonemetsClient.Where(x => x.Client.ID_Client == item.ID_Client && x.DateStart >= start && x.DateStart <= end).Sum(x => x.Abonement.Price).ToString();
                row.AmounServ = row.AmounServ == "0" ? "0" :
                    context.JournalServices.Where(x => x.AbonemetsClient.Client.ID_Client == item.ID_Client && x.DateUse >= start && x.DateUse <= end).Sum(x => x.Cost).ToString();

                row.Discount = GetDiscountClient(item).ToString();

                table.Rows.Add(row);
            }

            return table;
        }
        //Создает таблицу с расписание сотрудников
        public static SportDataSet.ScheduleTimeDataTable GetScheduleTimeTable()
        {
            return new SportDataSet.ScheduleTimeDataTable();
        }

        //удалить определенный день из расписания опр. сотрудника
        public static void DeleteScheduleDay(int ID_empl, string DayOfWeak)
        {
            var item = context.Schedule.Where(x => x.DayOfWeak == DayOfWeak && x.Employee.ID_Employee == ID_empl).FirstOrDefault();

            if (item != null)
            {
                context.DeleteObject(item);
                context.SaveChanges();
            }

        }
        //заморозить/разморозить абонемент
        internal static void ChangeStatusAbonement(AbonemetsClient abon)
        {
            context.SaveChanges();
        }
        //получить список тренажеров
        internal static List<Apparatus> GetApparatusList()
        {
           return context.Apparatus.ToList();
        }
        //обновить тренажер
        internal static void UpdateApparatus(Apparatus item)
        {
            context.SaveChanges();
        }
        //добавить тренажер
        internal static void InsertApparatus(Apparatus item)
        {
            context.AddToApparatus(item);
            context.SaveChanges();
        }
        //удалить тренажер
        internal static void DeleteApparatus(Apparatus item)
        {
            context.DeleteObject(item);
            context.SaveChanges();
        }
        //вернуть список тренажеров спортклуба
        internal static List<GumApparatus> GetGymAparatus()
        {
           return context.GumApparatus.ToList();
        }
        //обновить тренажер спортклуба
        internal static void UpdateGymApparatus(GumApparatus item)
        {
            context.SaveChanges();
        }
        //добавить тренажер спортклуба
        internal static void InsertGumApparatus(GumApparatus item)
        {
            context.AddToGumApparatus(item);
            context.SaveChanges();
        }
        //сменить пароль для сотрудника
        internal static void ChangePass(Employee _obj, string p)
        {
            _obj.Password = SecurityClass.GetSecurityPassword(p).ToString();
            context.SaveChanges();
        }
    }
}
