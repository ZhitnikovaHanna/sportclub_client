﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportClub_Client.Data
{
    partial class Post
    {
        //Часть класса с переопределенным методом ТоString() чтобы  отображался в визуальных элементах нормально
        public override string ToString()
        {
            return Title;
        }
    }
}
