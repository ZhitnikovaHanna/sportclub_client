﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportClub_Client
{
    //Класс для хранения времени работы (чтобы в каждой ячейке отображалось время, например 7:00 - 16:00 и чтобы это время потом легко было разбить на составляющие
    //начало и конец рабочего дня
    class ScheduleTime
    {
        public TimeSpan? start;
        public TimeSpan? end;

        public override string ToString()
        {
            return start.Value + " - " + end.Value;
        }
    }
}
