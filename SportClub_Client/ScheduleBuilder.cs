﻿using System.Data;
using SportClub_Client.Data;

namespace SportClub_Client
{
    //Класс для формирования расписания
    class ScheduleBuilder
    {
        static DataTable scedule;

        public static DataTable Scedule
        {
            get { BuildScedule(); return ScheduleBuilder.scedule; }
            set { ScheduleBuilder.scedule = value; }
        }
        static ScheduleBuilder()
        {
            BuildScedule();
        }

        //Сформировать таблицу расписания
        static void BuildScedule()
        {
            scedule = CreateTable(); //создать таблицу

            //для каждого сотрудника создаем строку
            foreach (var emp in DataAccess.GetEmployees())
            {
                //заполняем первые 2 колонки
                var row = scedule.NewRow();
                row["ФИО"] = emp;
                row["Должность"] = emp.Post.Title;

                //Далее для каждого дня недели заполняем соотв. колонку в таблице по названию дня недели
                foreach (Schedule _schedule in emp.Schedule)
                {
                    row[_schedule.DayOfWeak] = new ScheduleTime() { start = _schedule.TimeStart, end = _schedule.TimeEnd };
                }
                scedule.Rows.Add(row);
            }

        }

        //Создаем табл. где каждая колонка = день недели
        static DataTable CreateTable()
        {
            DataTable table = new DataTable();
            table.Columns.AddRange(new DataColumn[] 
            {
                new DataColumn("ФИО", typeof(Employee)),
                new DataColumn("Должность", typeof(string)),
                new DataColumn("Понедельник", typeof(ScheduleTime)),
                new DataColumn("Вторник", typeof(ScheduleTime)),
                new DataColumn("Среда", typeof(ScheduleTime)),
                new DataColumn("Четверг", typeof(ScheduleTime)),
                new DataColumn("Пятница", typeof(ScheduleTime)),
                new DataColumn("Суббота", typeof(ScheduleTime)),
                new DataColumn("Воскресение", typeof(ScheduleTime)),
            });
            return table;
        }
    }
}
