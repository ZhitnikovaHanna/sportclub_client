﻿using SportClub_Client.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SportClub_Client
{
    static class Program
    {
        static object People;
        static bool isOk;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var l = new LoginForm();
            l.FormClosing += L_FormClosing;
            l.FormClosed += L_FormClosed;
            Application.Run(l);
        }

        private static void L_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (isOk)
            {
                MainForm mf = new MainForm(People);
                mf.ShowDialog();
            }
        }

        private static void L_FormClosing(object sender, FormClosingEventArgs e)
        {
            var form = sender as LoginForm;
            People = form.People;
            isOk = form.isOK;
            form.Hide();
            form.ShowInTaskbar = false;
        }
    }
}
