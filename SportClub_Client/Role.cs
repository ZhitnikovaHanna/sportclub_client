﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportClub_Client
{
    //Класс для хранения роли пользователя
    public static class Role
    {
        public static RoleEnum role;
    }

    //Перечисление обозначающее роль
    public enum RoleEnum
    {
        Администратор = 0,
        Менеджер = 1
    }
}
